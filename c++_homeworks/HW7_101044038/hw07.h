/*
 * 
 * 
 * BIL241 C++  HW7
 * 
 * FATMA CELIK
 * 101044038
 * 
 * 
 * */
 
 
#ifndef HW07_H
#define HW07_H

#include <fstream>
#include <iostream>
#include <string>
#include <sstream> 
#include <cstdlib> 


using namespace std;


#define CLEAR_SCREEN  system("clear")


namespace FatmaCelik
{
		
	class DocumentIndex
	{
		public:
				DocumentIndex();
				DocumentIndex(string file);
				DocumentIndex(const DocumentIndex& doc);
							
				//getter
				int getSize()const;
				string getFileName() const;					
				
				void setFile(string name);
				
				///index operator
				string operator[](int line)const;
				string operator[](string word)const;
				string toString(int line)const;

				// search a word in words string
				int searchWord(string word)const;
				
				DocumentIndex& sort();
				
				//operator overloading
				DocumentIndex& operator+=(const DocumentIndex& doc);
				friend DocumentIndex operator+(const DocumentIndex& doc1,const DocumentIndex& doc2);
		
				friend ifstream& operator>>(ifstream& inStream, DocumentIndex& doc);
				friend ofstream& operator<<(ofstream& outStream, const DocumentIndex& doc);
		
				DocumentIndex& operator=(const DocumentIndex& doc);
				DocumentIndex& add(string word, string line);
				
				//destructor
				~DocumentIndex();
				
		private:
				int size;
				string **words;
				string fileName;
	}; // end class
	
	
}//namespace end






#endif
