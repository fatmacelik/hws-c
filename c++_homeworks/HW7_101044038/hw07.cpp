/*
 * 
 * 
 * BIL241 C++  HW7
 * 
 * FATMA CELIK
 * 101044038
 * 
 * 
 * */


#include "hw07.h"



namespace FatmaCelik
{
	/*
	 * Constructors
	 * */
	DocumentIndex::DocumentIndex(): size(0)
	{}
	
	DocumentIndex::DocumentIndex(string file) : size(0)
	{
		fileName = file;
	}
	
	/*
	 * copy constructors
	 * */
	DocumentIndex::DocumentIndex(const DocumentIndex& doc)
	{
		if( this->size != doc.size)
		{
			this->size = doc.size;
			
			this->words = new string*[size];
			
			for(int i=0; i<size ; ++i)
				this->words[i] = new string[2];		
		} 
		
		this->fileName = doc.fileName;
		for(int i=0; i<size ;++i)
		{
			this->words[i][0] = doc.words[i][0];
			this->words[i][1] = doc.words[i][1];
		}		
	}
	
	/*
	 *  set document file name
	 * */
	void DocumentIndex::setFile(string name)
	{
		fileName = name;
	}

	/*
	 * Getters
	 * */
	int DocumentIndex::getSize()const
	{
		return size;
	}
	
	
	string DocumentIndex::getFileName() const
	{
		return fileName;
	}

	
	/*
	 * operator overloading [] (line)
	 * 
	 * */
	string DocumentIndex::operator[](int line) const
	{
		if( line > 0 && line <= size)
		{
			return words[line-1][0];		
		}
		
		return words[0][0];
	}
	
	
	/*
	 * operator overloading [](word)
	 * 
	 * */
	string DocumentIndex::operator[](string word)const
	{
		for(int i=0; i<size; ++i)
			if( words[i][0] == word)
				return words[i][1];
				
		
		string notFound = "notFound";
		
		return notFound;
	}
	
	/*
	 * assignment operator overloading
	 * 
	 * */
	DocumentIndex& DocumentIndex::operator=(const DocumentIndex& doc)
	{
		if( this->size != doc.size)
		{
			this-> size = doc.size;
			
			this->words = new string*[size];
			for(int i=0; i<size ; ++i)
				this->words[i] = new string[2];		
		} 
		
		this->fileName = doc.fileName;
		
		for(int i=0; i<size ;++i)
		{
			this->words[i][0] = doc.words[i][0];
			this->words[i][1] = doc.words[i][1];
		}	
	
		return *this;
	}
	
	
	
	
	/*
	 * convert integer to string
	 * */
	string DocumentIndex::toString(int line)const
	{
		string str;
		ostringstream temp;
		
		temp<<line;
	
		return temp.str();
	}
	
	/*
	 * read words from document file
	 * store words and lines
	 *   
	 *
	 * */
	ifstream& operator>>(ifstream& inStream, DocumentIndex& doc)
	{
		int line;
		string lineStr;
		istringstream temp;
		string word;		
		
		inStream.open(doc.getFileName().c_str());

		int i;
		for ( line = 0; getline(inStream, lineStr); ++line)
		{		
			temp.str(lineStr);
			
			 for (i = 0; temp.good()  ; i++) 
			 {
				 temp >> word;
				 
				doc.add(word,doc.toString(line+1));				
		   	 }
			temp.clear(); 
		}
		
		inStream.close();
		
		return inStream;
	}
	

	/*
	 * write to words and lines in output file
	 * 
	 * */
	ofstream& operator<<(ofstream& outStream, const DocumentIndex& doc)
	{
		string file = "output-" + doc.getFileName();
		string temp;
		outStream.open(file.c_str());
		
		outStream << "INDEX\n" << "--------\n";
		temp =  doc.toString(doc.getSize());
		
		
		for(int i=0; i < doc.getSize() ; ++i)
		{
			temp = doc.words[i][0] + "     ............... " + doc.words[i][1] + "\n";
			outStream << temp.c_str();
		}
		
		outStream.close();
		
		return outStream;
	}
	
	/*
	 * sorting words in document objects
	 * 
	 * */
	DocumentIndex& DocumentIndex::sort()
	{
		 
		int i, j;
		string temp;
		 
		for (i = 1; i < this->size; i++) 
		{
			j = i;
			while (j > 0 && this->words[j-1][0] > this->words[j][0]) 
			{
				
					temp = this->words[j][0];
					this->words[j][0] = this->words[j-1][0];
					this->words[j-1][0] = temp;
					
					temp = this->words[j][1];
					this->words[j][1] = this->words[j-1][1];
					this->words[j-1][1] = temp;	
				
					j--;
				

			}//while
			
		}

		return *this;
	}



	/*
	 * += operator overloading, insert a other doument object in this object
	 * */
	DocumentIndex& DocumentIndex::operator+=(const DocumentIndex& doc)
	{	
		for(int i=0; i<doc.size; ++i)
			this->add(doc.words[i][0], doc.words[i][1]);
		
		return *this;
	}

	/*
	 * two documnet objects merged, and return new document object
	 * 
	 * */
	DocumentIndex operator+(const DocumentIndex& doc1,const DocumentIndex& doc2)
	{
		DocumentIndex temp;
		
		for(int i=0; i<doc1.size; ++i)
			temp.add(doc1.words[i][0],doc1.words[i][1]);
		
		for(int i=0; i<doc2.size; ++i)
			temp.add(doc2.words[i][0], doc2.words[i][1]);
		
		return temp;
	}

	/*
	 * adding a word in document string
	 * */
	DocumentIndex& DocumentIndex::add(string word, string line)
	{
		int index = searchWord(word);
		
		if( index >=0 )
		{		
			this->words[index][1] = this->words[index][1] + "," + line;
			return *this;
		}
	
		DocumentIndex temp(*this);
		
		this->size++;
		this->words = new string*[this->size];

		int i;
		for( i=0; i< this->size ; ++i)
		{
			this->words[i] = new string[2];

			if( i < this->size-1)
			{
				this->words[i][0] = temp.words[i][0];
				this->words[i][1] = temp.words[i][1];	
			}	
		}		

		this->words[i-1][0] = word;
		this->words[i-1][1] = line;

		this->sort();

		return *this;
	}
	
	
	
	/*
	 * searching a word in words string of this object
	 * if word is found, return index, other -1
	 * 
	 * */
	int DocumentIndex::searchWord(string word)const
	{
		for(int i=0; i<size ; ++i)
		{
			if( words[i][0] == word)
				return i;
		}
	
		return -1;
	}
	
	
	/*
	 * destructor
	 * */
	DocumentIndex::~DocumentIndex()
	{
		if(size != 0)
		{	
			for(int i=0 ; i<size ;++i)
			{
				delete [] words[i];
			}
			delete [] words;
		}
	}
	
	
	
} // namespace end
