/*
 * 
 * 
 * BIL241 C++  HW7
 * 
 * FATMA CELIK
 * 101044038
 * 
 * 
 * */

#include "hw07.h"


using namespace FatmaCelik;


int main()
{
	
	CLEAR_SCREEN;
	
	// created documentIndex objects
	DocumentIndex   doc1("doc1.txt"),
					doc2("doc2.txt"),
					doc3("doc3.txt");
	ifstream input;
	ofstream output;
	
	// read file and write file DocumentIndex objects
	input >> doc1 >> doc2 >> doc3;
	output << doc1 << doc2 << doc3;
	
	//test
	cout << "\n ***** [] operator overloading test **********\n";
	
	cout << "\n doc1.operator[](are)   :  " << doc1.operator[]("are");
	cout << "\n doc1.operator[](all)   :  " << doc1.operator[]("all");
	cout << "\n doc1.operator[](bit)   :  " << doc1.operator[]("bit");
	cout << "\n doc1.operator[](call)  :  " << doc1.operator[]("call") << endl;

	cout << "\n doc2.operator[](fast)  :  " << doc2.operator[]("fast");
	cout << "\n doc2.operator[](also)  :  " << doc2.operator[]("also");
	cout << "\n doc2.operator[](local) :  " << doc2.operator[]("local");
	cout << "\n doc2.operator[](net)   :  " << doc2.operator[]("net") << endl;


	cout << "\n doc3.operator[](159)   :  " << doc3.operator[](159);
	cout << "\n doc3.operator[](134)   :  " << doc3.operator[](134);
	cout << "\n doc3.operator[](147)   :  " << doc3.operator[](147);
	cout << "\n doc3.operator[](180)   :  " << doc3.operator[](180) << endl;

	cout << "\n ***** += ,  +  operator overloading test **********\n";

	DocumentIndex   doc4("doc4.txt"),
					doc6("doc6.txt"),
					doc7("doc7.txt");

	doc7 = doc3 + doc2;
	doc7.setFile("doc7.txt");
	output << doc7;
	
	cout << "\n doc7 = doc3 + doc2";
	cout << "\n Result test: Created new output file : Look <output-doc7.txt> \n";

	 
	doc6 += doc1;
	doc6.setFile("doc6.txt");
	output << doc6;
	
	cout << "\n doc6 += doc1";
	cout << "\n Result test: Created new output file : Look <output-doc6.txt> \n";

	 
	doc6 += doc3;
	doc6.setFile("doc6--2.txt");
	output << doc6;
	
	cout << "\n doc6 += doc3";
	cout << "\n Result test: Created new output file : Look <output-doc6--2.txt> \n\n";

	doc4 = doc3;
	doc4.setFile("doc4.txt");
	output << doc4;

	cout << "\n doc4 = doc3";
	cout << "\n Result test: Created new output file : Look <output-doc4.txt> \n\n";
	
	
	DocumentIndex doc5(doc1);
		
	doc5.setFile("doc5.txt");
	output << doc5;

	cout << "\n copy constructor, doc5(doc1)";
	cout << "\n Result test: Created new output file : Look <output-doc5.txt> \n";
	
	cout << "\n *****************************************************************\n\n";
	 
	return 0;
}
