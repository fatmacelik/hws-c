
/* **************************************
 *  CSE 241 - HW1
 * 
 *  4 OCTOBER 2014
 * 
 * FATMA CELIK  / 101044038
 * 
 * GAME OF LIFE
 * 
 * ************************************** */
 
 
#ifndef MAIN
#define MAIN

	#include "game.h"
	using namespace std;


	int main(int argc, char* argv[])
	{
		char name[FILE_SIZ] = "",
			 file[FILE_SIZ] = "../boards/",
			 text[6] = ".txt" ,
			 temp[FILE_SIZ];
			 
		int N=0;

		CLEAN_SCREEN;
		
		cout << "\n Hello! Welcome Game of Life.."
			 << "\n\n Boards file names:\n  "
			 << "\n board0   board1    board2   board3    board4 "
			    "\n board5   board6    board7   board8    board9 \n"
			 << "\n Please, enter filename:  ";
		cin >>	 name;
		cout << " Please, enter N step number:  ";
		cin >> temp;
		
		N = checkNum(temp);
		if(N == 0)
		{
			cout << "\n ----------"
					"\n Please check N step number.. Exiting\n"
					"\n ----------\n\n";			
			return 0;
		}
			
		strcat(file,name);
		strcat(file,text);
				
		playGame(file,N);
	
		return 0;
	}


#endif
