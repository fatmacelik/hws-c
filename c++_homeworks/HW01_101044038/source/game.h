/* **************************************
 *  CSE 241 - HW1
 * 
 *  4 OCTOBER 2014
 * 
 * FATMA CELIK  / 101044038
 * 
 * GAME OF LIFE
 * 
 * ************************************** */
 

#ifndef GAME_H
#define GAME_H

	//***************** INCLUDES ***************************
	#include <iostream>
	#include <cstdlib>  // malloc
	#include <cstdio>   // fscanf, fprintf
	#include <cstring>  // strcat
	
	#define FILE_SIZ 50
	#define CLEAN_SCREEN system("clear")

	// ************* FUNCTION PROTOTYPES *******************
	
	// convert string to int
	int myAtoi(char *arr);
	
	// check char array, if arr have just numbers, return 1 
	int checkNum(char *arr);
	
	// if ch is num, return 1
	int isNum(char ch);

	// read size from file
	int readSizeFromFile(FILE *inp);
	
	//free for malloc
	void Free(char **arr, int size);
	
	//printed board on screen
	void printBoard(char **board, int size);
	
	//read board from given file name by user
	int readBoardFromFile(FILE* inp, char **board, int size);
	
	// game function, all operations are in this function
	void playGame(char file[FILE_SIZ], int N);
	
	//copy a board into a new board array
	void copyFromBoardToNewBoard(char **fromBoard, char **toNewBoard, int size);
	
	// invalid characters changed as blank character
	int checkChars(char **board, int size);
	
	// finding index of all neighbors for a cell
	void findIndexofNeig( int neigs[8][2], int row, int col, int size);
	
	// finding number of neighbors for a cell
	int  findNumOfNeig(char **board, int neigs[8][2], int size);	
	
	// apply all rules on board and occur a new board
	void applyRules(char **board, int size, char** newBoard);
	
	// print finish game message and ask new file name
	void finishGame(char **board, int size);
	
	// compare two boards, if same return 1, other return 0
	int  checkSameBoard(char **board1, char **board2, int size);
	
	// play game N times, user enter and go new step until N times
	void playNstep( char** preBoard, char** board, char** newBoard, int size, int N);

#endif

//#######################################################################
