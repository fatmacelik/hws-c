/* **************************************
 *  CSE 241 - HW1
 * 
 *  4 OCTOBER 2014
 * 
 * FATMA CELIK  / 101044038
 * 
 * GAME OF LIFE
 * 
 * ************************************** */

#ifndef GAME_CPP
#define GAME_CPP

	#include "game.h"
	using namespace std;

	/* ************************************************************
	 * Operations:
	 * 				opened/closed board file
	 * 				malloc for board array
	 * 				call readBoardFromFile function
	 * 				call printBoard function
	 * 				free malloc
	 * ********************************************************** */
	void playGame(char file[FILE_SIZ], int N)
	{
		char **preBoard, **board, **newBoard; // board array
		FILE* inp;    // board file
		int size=0,i=0; // board size, index
		int status;
		
		inp = fopen(file,"r"); // opened file
		if(inp != NULL){			
			size = readSizeFromFile(inp); // read size from file
			
			if( size != 0)
			{
				board = (char**)malloc(size * sizeof(char*));// malloc for board array
				newBoard = (char**)malloc(size * sizeof(char*));
				preBoard = (char**)malloc(size * sizeof(char*));
				for(i=0; i<size;++i){
					board[i]= (char*)malloc(size*sizeof(char));
					newBoard[i]= (char*)malloc(size*sizeof(char));
					preBoard[i]= (char*)malloc(size*sizeof(char));
				}						
				status = readBoardFromFile(inp,board,size); //read board and size from file	
				fclose(inp);	// closed file
				
				if( status == 1){							
					// play game N times
					playNstep(preBoard,board, newBoard, size, N);
					cout << "\n Exiting!"
						 << "\n\n **************************************************\n\n";					
				}
				else
					cout << "\n ------------ \n\n Please check board file..  Exiting..\n "
				            "\n ------------ \n\n" ;
														
				Free(board,size);	// free for malloc
				Free(newBoard,size);	// free for malloc
				Free(preBoard,size);	// free for malloc
			}
			else{
				cout << "\n ------------ \n\n Board size is invalid.. Please check size!  Exiting..\n "
				        "\n ------------ \n\n" ;								
				fclose(inp);
				}
			
		}else
			cout << "\n Not opened " << file << " file!\n Exiting..\n\n" ;								
	}
	
	
	/* ************************************************************
	 * Operations:
	 * 				read board size from file
	 * ********************************************************** */
	int readSizeFromFile(FILE *inp)
	{
		char temp[FILE_SIZ]="";
		int size=1;
		fscanf(inp,"%s",temp);
		
		if(temp[0] == '\0')
			return 0;
		
		size = checkNum(temp);
		
		return size;
	}

	/* ************************************************************
	 * Operations:
	 * 	 			if char array  have just  number characters,
	 *                                   return 1, other 0
	 * ********************************************************** */
	int checkNum(char *arr)
	{
		int i, status,size=0;
		
		for(i=0; arr[i] != '\0' ; ++i)
		{
			status = isNum(arr[i]);
			if(status == 0)
				return 0;
		}
		
		size = myAtoi(arr);
		
		return size;
	}
	
	/* ************************************************************
	 * Operations:
	 * 	 			if ch is num, return 1, other return 0
	 * ********************************************************** */
	int isNum(char ch)
	{
		char nums[12] = "0123456789";
		int i;
		
		for(i=0; nums[i] != '\0'; ++i){
			if(ch == nums[i])
				return 1;
		}	
		return 0;
	}
	
	/* ************************************************************
	 * Operations:
	 * 	 			cnvert string to int
	 * ********************************************************** */	
	int myAtoi(char *arr)
	{
		int i,num=0;
		
		for(i=0; arr[i] != '\0' ; ++i)
			num = (num *10) + (arr[i] - '0');
		
		return num;
	}

	
	/* ************************************************************
	 * Operations:
	 * 	 			call copyFromBoardToNewBoard function
	 * 				call applyRules function
	 * 
	 * 				user playing game of life
	 * 	            user enter '\n' character and go new step until N step
	 *              if step is N, ask user to new file name for printing new board 
	 * ********************************************************** */
	void playNstep( char** preBoard, char** board, char** newBoard, int size, int N)
	{
		char ch;
		int status1=0, status2=0,flag=1,k;
		
			copyFromBoardToNewBoard(board, newBoard,size);
			copyFromBoardToNewBoard(board, preBoard,size);
			cin.get(ch); 
			printBoard(board,size);
			for(k=1; k<=N && flag; ++k)
			{		
				cout << "\n Step" << k << " - Please enter to continue..";
				cin.get(ch); //  take an '\n' enter character		
				applyRules(board,size, newBoard);	
				printBoard(newBoard,size);				
	
				status1 = checkSameBoard(newBoard,board,size);				
				if(k>2)
					status2 = checkSameBoard(newBoard,preBoard,size); //check oscillator
				if(status1 == 1 || status2 == 1)		
				{
					flag = 0;
					cout << "\n " << k << ". step : Game finished...";	
					if(status1 == 1)				
						cout << "\n Same Board is repeated...";
					if(status2 == 1)
						cout << "\n This Board is Oscillators! \n";
						
					finishGame(newBoard,size);					     
				}
				else
				{				
					copyFromBoardToNewBoard(board, preBoard,size);
					copyFromBoardToNewBoard(newBoard, board,size);
				}
			}
			
			if(flag){								
				cout << "\n In step " << N << ",  Game finished...\n";
				finishGame(newBoard,size);
			}	
		
	}
	
	
	
	
	/* ************************************************************
	 * Operations:
	 * 				comparing two board
	 * ********************************************************** */	
	int  checkSameBoard(char **board1, char **board2, int size)
	{
		int i,k;
		
		for(i=0; i<size; ++i)
		{
			for(k=0; k<size; ++k)
			{
				if(board1[i][k] != board2[i][k])
					return 0; // not same two board
			}	
		}
		
		return 1;	// same two board
	}

	/* ************************************************************
	 * Operations:
	 * 				Print finish message
	 * 				ask user file name for new board 
	 * ********************************************************** */
	void finishGame(char **board, int size)
	{
		FILE *outp;
		int i,k;
				
		char name[FILE_SIZ] = "",
			 file[FILE_SIZ] = "../newBoards/";
			 			 
		cout << "\n Please, enter a file name to print new board.."	
			 << "\n Enter file name:  ";
		cin >> name;	
		
		strcat(file,name);
				
		outp = fopen(file,"w");	//opened file
		
		cout << "\n Created new board file: " << file << endl; 
		fprintf(outp,"Board size: %d x %d\n",size,size);
		
		for(i=0; i< size; ++i){
			for(k=0; k<size; ++k)
			{
				if(board[i][k] != 'X')
					fprintf(outp,"_"); // blank
				else
					fprintf(outp,"X");
			}			
			fprintf(outp,"\n");
		}
		
		fclose(outp);	// closed file	
	}
	
	
	/* ************************************************************
	 * Operations:
	 * 				 game of life rules are applied in board and
	 *               stored new board
	 * ********************************************************** */
	void applyRules(char **board, int size, char** newBoard)
	{
		int i,k;
		int neigs[8][2]; // array is indexs of neighbors
		int countNeig=0;
		
		for(i=0; i<size ; i++)
		{
			for(k=0; k<size; ++k){
				findIndexofNeig(neigs, i, k,size);
				countNeig = findNumOfNeig(board,neigs, size);
		
				if( (countNeig == 2  || countNeig == 3) && board[i][k] == 'X')
					newBoard[i][k] = 'X';				
				else if(  countNeig == 3 && board[i][k] == ' ')
					newBoard[i][k] = 'X';					
				else 
					newBoard[i][k] = ' ';				
			}
		}//end for
	}


	/* ************************************************************
	 * Operations:
	 *              stored index of all neighbors into neigs[8][2] array
	 *              there are 8 neighbors
	 * ********************************************************** */
	void findIndexofNeig( int neigs[8][2], int row, int col , int size)
	{
		int k;
		int temp[8][2] = {  {row-1,col-1}, {row-1,col}, {row-1,col+1},
							{row,col-1},                {row,col+1},
							{row+1,col-1}, {row+1,col}, {row+1,col+1}   
						};			
		
		for(k=0; k<8 ;++k)
		{			
			neigs[k][0]= temp[k][0];
			neigs[k][1]= temp[k][1];		
		}					
	}
	
	
	/* ************************************************************
	 * Operations:
	 * 				finding all neighbors of a cell,
	 *              return count of neighbors
	 * ********************************************************** */
	int findNumOfNeig(char **board, int neigs[8][2], int size)
	{
		int count=0;
		int k,row,col; // index
				
		for(k=0; k<8; ++k)
		{
			row = neigs[k][0];
			col = neigs[k][1];
			
			if(row >=0 && row < size && col >=0 && col < size)
			{
				if( board[row][col] == 'X')
					count++;			
			} 
		}
		
		return count;
	}


	/* ************************************************************
	 * Operations:
	 * 				invalid charcater changed as blank character
	 * ********************************************************** */
	int checkChars(char **board, int size)
	{
		int i,k;
		
		for(i=0; i<size ; i++)
		{	for(k=0 ; k<size ; ++k)
			{
				if(board[i][k] != 'X' && board[i][k] != '_')				
					return 0;
				
				if(board[i][k] == '_')
				     board[i][k] = ' ';
			}	}	
			
		return 1;	
	}


	/* ************************************************************
	 * Operations:
	 * 				copy a board into a new board
	 * ********************************************************** */
	void copyFromBoardToNewBoard(char **fromBoard, char **toNewBoard, int size)
	{
		int i,j;
		
		for(i=0; i<size;++i)
			for(j=0; j<size;++j)
				toNewBoard[i][j] = fromBoard[i][j];
	}


	/* ************************************************************
	 * Operations:
	 * 				free function for 2d char arrays
	 * ********************************************************** */
	void Free(char **arr, int size)
	{
		int i=0;
		
		for(i=0; i<size;++i)
			free(arr[i]);
		free(arr);	
		
	}
	
	
	/* ************************************************************
	 * Operations:
	 * 				printing board on terminal screen
	 * ********************************************************** */
	void printBoard(char **board, int size)
	{
		int i,j;
		char blank[] = "         ";
		
		cout << "\n\n" << blank;
		cout << "Size: " << size << " x " << size << endl << blank;		
		for(i=0; i<size+2;++i)	cout << "-";
		
		cout << endl;		
		for(i=0; i<size ; ++i)
		{
			cout << blank << "|";
			for(j=0; j < size ; ++j)
				cout << board[i][j];
			
			cout << "|" << endl;
		}	
		cout << blank;
		for(i=0; i<size+2;++i)
			cout << "-";
			
		cout << "\n\n **************************************************\n";
	}


	/* ************************************************************
	 * Operations:
	 * 				read a board from given file name by user
	 *              store board into 2d char array
	 * ********************************************************** */
	int readBoardFromFile(FILE* inp, char **board, int size)
	{
		int i,j;
		char *temp;
		
		temp = (char*)malloc((size+2) * sizeof(char));					
		for(i=0; i<size;++i)
		{
			fscanf(inp,"%s",temp);
			
			if( temp[0] == '\0')
				return 0;
				
			for(j=0; j<size && temp[0] != '\0'; ++j)				
				board[i][j] = temp[j];
			
			temp[0] =  '\0';								
		}				
		free(temp);						
				
		return checkChars(board,size); // checked characters in board array
								// and invalid characters are changed as blank character		
	}


#endif

//#######################################################################

