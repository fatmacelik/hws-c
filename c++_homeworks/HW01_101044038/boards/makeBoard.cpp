/*
 * 
 * CSE241 HW01
 * 
 * FATMA CELIK  101044038
 * 
 * 
 * GAME OF LIFE BOARD
 * 
 * */
 
 
 
#include <iostream>
#include <cstdio>
#include <cstdlib> // rand

using namespace std;

#define CLEAR_SCREEN system("clear")
#define RANDMAX 25


void fillFile(FILE* outp);


int main()
{
	int i;
	int numOfFile=10;
	FILE* outp;
	char board[20]= "board-.txt";
	
	CLEAR_SCREEN;
	srand(time(NULL));

	cout << "\n *************************************\n";
	cout << "\n Hello! Welcome Make Board!\n ";
	
	for(i=0; i<numOfFile ; ++i){		
		board[5] = (i) + '0';
		outp = fopen(board,"w");
		
		fillFile(outp);		
		fclose(outp);
	}
	
	cout << "\n 10 board files are created. "
			"\n Please check <boards directory>..."
			"\n Bye Bye.. \n\n";

	cout << "\n *************************************\n";
	
	return 0;
}


void fillFile(FILE* outp)
{
	int i,k;
	int size = rand() % RANDMAX + 5;
	char chars[] = "X_";
	int index;
			
	fprintf(outp,"%d\n",size);		
	for(i=0; i< size; ++i)
	{
		for(k=0; k< size; ++k)
		{
			index = rand() % 2;
			fprintf(outp,"%c",chars[index]);
		}			
		fprintf(outp,"\n");
	}//for end	
}

