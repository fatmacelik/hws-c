/* *
 * 
 * BIL241 C++ HW05
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * 
 *  header 
 * */
 
 
#ifndef HW05_H
#define HW05_H

	#include <iostream>
	#include <cstdio> // fclose, fopen
	#include <cstdlib>
	#include <vector>
	#include <cstring> // strcat
	
	using namespace std;

	#define CLEAR_SCREEN system("clear")
	#define FILE_SIZ 50

	

	//Cell class
	class Cell
	{
		public:
			//constructors
			Cell();
			Cell(int _x, int _y);
			Cell(int _x, int _y, char c);

			//setters
			void setX(int _x);
			void setY(int _y);
			void set(int _x, int _y);
			void setCell(char c);
			
			//getters
			int getX() const { return x; }
			int getY() const { return y; }
			char getCell() const { return cell; }  
			
			//compares two cells
			bool operator <(const Cell& other) const;
			bool operator >(const Cell& other) const;
			bool operator <=(const Cell& other) const;
			bool operator >=(const Cell& other) const;
			bool operator ==(const Cell& other) const;
			bool operator !=(const Cell& other) const;

			// incerement decrement
			Cell operator++(int ignore); // postfix
			Cell operator++(); // prefix
			Cell operator--(int ignore); // postfix
			Cell operator--(); // prefix
			
			// stream overload
			friend ostream& operator<<(  ostream& out, const Cell& obje);
			friend istream& operator>>(  istream& inp,  Cell& obje);
				
		private:
			int x;
			int y;	
			char cell;
	};
	

	
	//Game of life class
	class GameOfLife
	{
		public:
		
			//constructors
			GameOfLife();
			GameOfLife(int w, int h, int n);
						
			//setters
			void setWidth(int w);
			void setHeight(int h);
			void setCell(int h, int w, char c);
			void setFile(char* str) ;
			void setN(int n) ;
			//getters
			char getCell(int h, int w) const { 		
				return livingCells[h*width+w].getCell();
			}
			
			char* getFileName()       { return fileName; }
			int getN() const         { return N; }
			int getWidth() const     { return width; }
			int getHeight() const    { return height; }			
			int numOfLivingCell()    { return numLivingCell; }					
					
			int findNumLivingCell() const;		
					
			//output
			void displayBoard();
			
			//read & write file
			int  readFile(FILE* inp);
			int  readSizeFromFile(FILE *inp);
			void writeFile(FILE* outp) const;	
			void play(char file[FILE_SIZ], int N);	// play function		
			int  checkChars(); 	// invalid charcaters checked			
			void playNstep();	// gmae played N step				
			void copyFromBoardToNewBoard(vector<Cell> newBoard);	// copy from newBoard to current board		
			void applyRules(vector<Cell>& newBoard);	// apply board game rules			
			void findIndexofNeig( int neigs[8][2], int row, int col) const;	// find index of neighbors			
			void finishGame() const;	// game finish				
			int  checkSameBoard(vector<Cell> newBoard) const;	// two board compared		
			int  findNumOfNeig( int neigs[8][2]) const;	// finding number of neighbors for a cell		
			void join( const GameOfLife& otherGame);	//alive cells of other game joined to current game
		
		
			/* **** operator overloading ***** */
			GameOfLife operator++(int ignore); //postfix
			GameOfLife operator++(); // prefix
			GameOfLife operator--(int ignore); //postfix
			GameOfLife operator--(); // prefix
			
			GameOfLife& operator+(const Cell& c); // return new GameOflife obje
			GameOfLife& operator-(const Cell& c); // return new GameOflife obje
			
			GameOfLife operator+(const GameOfLife& other) const; // return new GameOflife obje
			GameOfLife operator-(const GameOfLife& other) const; // return new GameOflife obje

			Cell operator[](int index); 
			Cell operator()(int index);

			GameOfLife& operator+=( const GameOfLife& other);

			friend ostream& operator<<(ostream& out, const GameOfLife& obje);

		private:
			vector<Cell> livingCells;
			int width;
			int height;
			int N; // step
			static int numLivingCell; // number of living cells
			char fileName[FILE_SIZ];
	};
	

	// convert string to int
	int myAtoi(char *arr);
	
	// check char array, if arr have just numbers, return 1 
	int checkNum(char *arr);
	
	// if ch is num, return 1
	int isNum(char ch);
	
	void fillBoard(GameOfLife& board, char* file);
	void test(GameOfLife& board1, GameOfLife& board2, 
	           GameOfLife& board3, GameOfLife& board4, 
	           GameOfLife& board5);
	
#endif


////##############################################################


