/* *
 * 
 * BIL241 C++ HW05
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * main implementation
 * 
 * */
 
 
#include "hw05.h"


	/*
	 * 
	 *  main functions 
	 * 
	 * */
	int main()
	{	
		char file1[FILE_SIZ] = "./boards/board1.txt",
			 file2[FILE_SIZ] = "./boards/board2.txt",
			 file3[FILE_SIZ] = "./boards/board3.txt",
			 file4[FILE_SIZ] = "./boards/board4.txt",
			 file5[FILE_SIZ] = "./boards/board5.txt";		 				 
				
		GameOfLife board1, board2, board3, board4, board5;
		
		CLEAR_SCREEN;
				
		// fill boards from file and set N step random		
		fillBoard(board1,file1); board1.setN(148);
		fillBoard(board2,file2); board2.setN(120);
		fillBoard(board3,file3); board3.setN(15);
		fillBoard(board4,file4); board4.setN(256);
		fillBoard(board5,file5); board5.setN(68);


		// test  ostream operator overloading
		cout << "Board1: " << board1 
			 << "Board2: " << board2 
			 << "Board3: " << board3 
			 << "Board4: " << board4 
			 << "Board5: " << board5;

		test(board1, board2, board3, board4, board5);
		
		return 0;
	}
	
	
	/*
	 *  test of gameoflife and cell objects with operator overloading methods
	 * 
	 * 
	 * */
	void test( GameOfLife& board1, GameOfLife& board2, GameOfLife& board3, 
	                   GameOfLife& board4,  GameOfLife& board5)
	
	{
		GameOfLife new1, new2; // test objects
		Cell c1(4,5,'X'), c2(2,3, 'X'), c3(10,2,' ');  // test cell objects
		
		
		// test operator += overloading
		board1 += board2;
		board3 += board4;
		
		// test operator ++, --
		board1++;
		++board1;
		board2--;
		board3--;
		--board4;
		board5++;
		++board5;
		
		// print new boards on screen
		// test  ostream operator overloading
		cout << " New Board1: " << board1 
			 << " New Board2: " << board2 
			 << " New Board3: " << board3 
			 << " New Board4: " << board4 
			 << " New Board5: " << board5;
		
		
		// test operator + , - overloading arguman cell object
		new1 = board1 + c1;
		new2 = board4 + c2;	
		cout << " New Board1: " << new1 
			 << " New Board2: " << new2;
				
		new1 = board2 - c1;
		new2 = board3 - c2;	
		cout << " New Board2: " << new1 
			 << " New Board3: " << new2;
			 
			 
		// test operator + ,  - arguman Gameoflife object
		new1 = board4 + board3;
		new2 = board1 + board2;
		cout << " New Board1: " << new1 
			 << " New Board2: " << new2;
		
		board2++;
	 	board1++;

		new1 = board2 - board1;
		new2 = board3 - board4;
		cout << " New Board1: " << new1 
			 << " New Board2: " << new2;
		
	
	
		//// cell test
		cout << "\n ******** CELL TEST **************\n";
		// test of operator index overloading ,  []  () 
		cout << " Cell board3[7]  : "  << board3[7] << endl;
		cout << " Cell board5[12] : "  << board5[12] << endl;
		cout << " Cell board4[2]  : "  << board4[2] << endl;
		cout << " Cell board1(14) : "  << board1(14) << endl;
		cout << " Cell board3(16) : "  << board3(16) << endl;
		
		
		//******** CELL ++ , -- TESTS **************
		c1++;
		++c2;
		--c3;
		c1--;
		
		
		
		cout << "\n ******** CELL COMPARE TESTS **************\n";
		if( c1 > c2)  cout << "c1 > c2  : true\n";
		else          cout << "c1 > c2  : false\n";
		
		if( c1 < c3)  cout << "c1 < c3  : true\n";
		else          cout << "c1 < c3  : false\n";
		
		if( c3 == c2) cout << "c3 == c2 : true\n";
		else          cout << "c3 == c2 : false\n";
		
		if( c1 != c2) cout << "c1 != c2 : true\n";
		else          cout << "c1 != c2 : false\n";
		
		if( c1 >= c2) cout << "c1 >= c2 : true\n";
		else          cout << "c1 >= c2 : false\n";
		
		if( c3 <= c1) cout << "c3 <= c1 : true\n";
		else          cout << "c3 <= c1 : false\n";	
		
		cout << endl << endl;
	}
	
	
	
	/*
	 * fill boards from file
	 * 
	 * */	
	void fillBoard(GameOfLife& board, char* file)
	{
		FILE* inp;    // board file
		
		board.setFile(file);
		inp = fopen(file,"r"); // opened file

		board. readSizeFromFile(inp);
							
		board.readFile(inp); //read board and size from file	
		
		fclose(inp);	// closed file
	}
	
