/* *
 * 
 * BIL241 C++ HW05
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 *  Cell class implementation
 * 
 * */
 
 
 
#include "hw05.h"


/************************ Cell Class ***************************/
	
	//constructors
	Cell::Cell(): x(0), y(0)
	{/*empty body*/}
	
	Cell::Cell(int _x, int _y)
	{
		if(_x >= 0)   x = _x;
		else	x = 0;
		
		if( _y >= 0)  y = _y;
		else  y =0;
	}
	
	Cell::Cell(int _x, int _y, char c)
	{
		if(_x >= 0)   x = _x;
		else	x = 0;
		
		if( _y >= 0)  y = _y;
		else  y =0;
		
		cell = c;
	}


	//setters
	void Cell::setCell(char c)
	{
			cell = c;
	}

	void Cell::setX(int _x) 
	{ 
		if(_x >= 0)   
			x = _x; 
	}
	void Cell::setY(int _y) 
	{ 
		if(_y >= 0)   
			y = _y;
	}
	
	void Cell::set(int _x, int _y)
	{
		if(_x >= 0)  
			x = _x;
				
		if( _y >= 0)  
			y = _y;		
	}
	

	/* ******************  compares two cells ********************** */
	// small
	bool Cell::operator <(const Cell& other) const 
	{
		if( y < other.y)
			return true;
		else if ( y == other.y)
		{
			if(x < other.x)
				return true;
		}	
		
		return false;
	}
	
	// big
	bool Cell::operator >(const Cell& other) const 
	{
		
		if( y > other.y)
			return true;
		else if ( y == other.y)
		{
			if(x > other.x)
				return true;
		}	
		
		return false;
	}
	
	// small or equal
	bool Cell::operator <=(const Cell& other) const 
	{		
		return  !(*this > other);
	}
	
	// big or equal
	bool Cell::operator >=(const Cell& other) const 
	{
		return !(*this < other);
	}
	
	// equal
	bool Cell::operator ==(const Cell& other) const  
	{
		if( x == other.x  && y == other.y)
			return true;
			
		return false;
	}
	
	// not equal
	bool Cell::operator !=(const Cell& other) const  
	{
		return !(*this == other);
	}


	/* **************** incerement decrement ********************** */
	Cell Cell::operator++(int ignore) // postfix
	{
		int tempX = x, tempY = y;
		
		x++;	y++;
		
		return Cell(tempX,tempY);
	}
	
	
	Cell Cell::operator++() // prefix
	{
		x++;	y++;
		
		return Cell(x,y);	
	}
	
	Cell Cell::operator--(int ignore) // postfix
	{
		int tempX = x, tempY = y;
		
		x--;	y--;
		
		return Cell(tempX,tempY);	
	}
	
	Cell Cell::operator--() // prefix
	{
		x--;	y--;
		
		return Cell(x,y);	
	}
	
	
	
	/* *********************** stream ***************************** */
	ostream& operator<<( ostream& out, const Cell& obje)
	{
		out << obje.cell;
		
		//ekrana index leri yazdirmiyorum
		//out << obje.x;
		//out << obje.y;
		
		return out;	
	}
	
	istream& operator>>( istream& inp,  Cell& obje)
	{
		inp >> obje.cell;
		inp >> obje.x;
		inp >> obje.y;
		
		return inp;
	}


/* ############################################################## */
