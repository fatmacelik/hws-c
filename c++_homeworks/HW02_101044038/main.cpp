/**
 * 
 * BIL241 C++ HW02
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
#include "hw02.h"

/*
 * Main function. Call testing functions
 * */
int main()
{
	// different types arrays
	int arrInt[SIZE] = {10,30,80,50,90,40,16};
	double arrDb[SIZE] = {-5.1, 2.8, 0, 1.5, 10.5, 2.7, -6.7};
	
	Person p1(4),p2(56),p3(87),p4(23),p5(32), p6(45),p7(90);
	Person persons[SIZE] = {p1,p2,p3,p4,p5,p6,p7};
	
	DayOfYear d1(4),d2(24),d3(23),d4(12),d5(19), d6(1),d7(5);
	DayOfYear days[SIZE] = {d1,d2,d3,d4,d5,d6,d7};
	
	
	CLEAR_SCREEN;
	
	
	
	// call test functions
	testInt(arrInt);
	testDb(arrDb);
    testPerson(persons);
    testDayOfYear(days);
    
    cout << "\n\n ****************************** \n\n";
	return 0;
}


/*
 * DayOfYear Array tested
 * 
 * */
void testDayOfYear(DayOfYear days[SIZE])
 {	 
	DayOfYear *day;	
	char name[NAME_SIZE] = "Days 0f DayOfYear ";

	printArray(days, SIZE, sizeof(DayOfYear), name );
    	
	day = (DayOfYear*)return_nth(days, SIZE, sizeof(DayOfYear),1, compDayOfYear);
	if(day != NULL)
		cout <<  "  1. day in DayOfYear array : " << *day << endl;
		
	day = (DayOfYear*)return_nth(days, SIZE, sizeof(DayOfYear),2, compDayOfYear);
	if(day != NULL)
		cout <<  "  2. day in DayOfYear array : " << *day << endl;
		
	day = (DayOfYear*)return_nth(days, SIZE, sizeof(DayOfYear),3, compDayOfYear);
	if(day != NULL)
		cout <<  "  3. day in DayOfYear array : " << *day << endl;
	
	day = (DayOfYear*)return_nth(days, SIZE, sizeof(DayOfYear),4, compDayOfYear);
	if(day != NULL)
		cout <<  "  4. day in DayOfYear array : " << *day << endl;
		
	day = (DayOfYear*)return_nth(days, SIZE, sizeof(DayOfYear),5, compDayOfYear);
	if(day != NULL)
		cout <<  "  5. day in DayOfYear array : " << *day << endl;
		
	day = (DayOfYear*)return_nth(days, SIZE, sizeof(DayOfYear),6, compDayOfYear);
	if(day != NULL)
		cout <<  "  6. day in DayOfYear array : " << *day << endl;
		
	day = (DayOfYear*)return_nth(days, SIZE, sizeof(DayOfYear),7, compDayOfYear);
	if(day != NULL)
		cout <<  "  7. day in DayOfYear array : " << *day << endl;
}



/*
 * Person Array tested
 * 
 * */
void testPerson(Person persons[SIZE])
 {	 
	Person *per;	
	char name[NAME_SIZE] = "Ages of Persons";

	printArray(persons, SIZE, sizeof(Person), name );
    	
	per = (Person*)return_nth(persons, SIZE, sizeof(Person),1, compPerson);
	if(per != NULL)
		cout <<  "  1. Person Age in person array : " << *per << endl;
 
	per = (Person*)return_nth(persons, SIZE, sizeof(Person),2, compPerson);
	if(per != NULL)
		cout <<  "  2. Person Age in person array : " << *per << endl;
 
	per = (Person*)return_nth(persons, SIZE, sizeof(Person),3, compPerson);
	if(per != NULL)
		cout <<  "  3. Person Age in person array : " << *per << endl;
		
	per = (Person*)return_nth(persons, SIZE, sizeof(Person),4, compPerson);
	if(per != NULL)
		cout <<  "  4. Person Age in person array : " << *per << endl;

	per = (Person*)return_nth(persons, SIZE, sizeof(Person),5, compPerson);
	if(per != NULL)
		cout <<  "  5. Person Age in person array : " << *per << endl;
		
	per = (Person*)return_nth(persons, SIZE, sizeof(Person),6, compPerson);
	if(per != NULL)
		cout <<  "  6. Person Age in person array : " << *per << endl;
		
	per = (Person*)return_nth(persons, SIZE, sizeof(Person),7, compPerson);
	if(per != NULL)
		cout <<  "  7. Person Age in person array : " << *per << endl;
}

/*
 * Integers Array tested
 * 
 * */
void testInt(int arrInt[SIZE])
 {	 
	int *numInt;	
	char name[NAME_SIZE] = "Integer";

	printArray(arrInt, SIZE, sizeof(int), name );
    	
	numInt = (int *)return_nth(arrInt, SIZE, sizeof(int),1, compInt);
	if(numInt != NULL)
		cout <<  "  1. integer num in array : " << *numInt << endl;
 
	numInt = (int *)return_nth(arrInt, SIZE, sizeof(int),2, compInt);
	if(numInt != NULL)
		cout <<  "  2. integer num in array : " << *numInt << endl;
	
	numInt = (int *)return_nth(arrInt, SIZE, sizeof(int),3, compInt);
	if(numInt != NULL)
		cout <<  "  3. integer num in array : " << *numInt << endl;
	
	numInt = (int *)return_nth(arrInt, SIZE, sizeof(int),4, compInt);
	if(numInt != NULL)
		cout <<  "  4. integer num in array : " << *numInt << endl;
	
	numInt = (int *)return_nth(arrInt, SIZE, sizeof(int),5, compInt);
	if(numInt != NULL)
		cout <<  "  5. integer num in array : " << *numInt << endl;
 	
 	numInt = (int *)return_nth(arrInt, SIZE, sizeof(int),6, compInt);
	if(numInt != NULL)
		cout <<  "  6. integer num in array : " << *numInt << endl;
 
	numInt = (int *)return_nth(arrInt, SIZE, sizeof(int),7, compInt);
	if(numInt != NULL)
		cout <<  "  7. integer num in array : " << *numInt << endl;
}


/*
 * Double Array tested
 * 
 * */
void testDb(double arrDb[SIZE])
 {	 
	double *numDb;	
	char name[NAME_SIZE] = "Double";

	printArray(arrDb, SIZE, sizeof(double), name );
          
	numDb = (double *)return_nth(arrDb, SIZE, sizeof(double),1, compDb);
	if(numDb != NULL)
		cout <<  "  1. double num in array : " << *numDb << endl;

	numDb = (double *)return_nth(arrDb, SIZE, sizeof(double),2, compDb);
	if(numDb != NULL)	
		cout <<  "  2. double num in array : " << *numDb << endl;
	
	numDb = (double *)return_nth(arrDb, SIZE, sizeof(double),3, compDb);
	if(numDb != NULL)
		cout <<  "  3. double num in array : " << *numDb << endl;
	
	numDb = (double *)return_nth(arrDb, SIZE, sizeof(double),4, compDb);
	if(numDb != NULL)
		cout <<  "  4. double num in array : " << *numDb << endl;
	
	numDb = (double *)return_nth(arrDb, SIZE, sizeof(double),5, compDb);
	if(numDb != NULL)
		cout <<  "  5. double num in array : " << *numDb << endl;
	
	numDb = (double *)return_nth(arrDb, SIZE, sizeof(double),6, compDb);
	if(numDb != NULL)
		cout <<  "  6. double num in array : " << *numDb << endl;
	
	numDb = (double *)return_nth(arrDb, SIZE, sizeof(double),7, compDb);
	if(numDb != NULL)
		cout <<  "  7. double num in array : " << *numDb << endl;
}



//##########################################################################
