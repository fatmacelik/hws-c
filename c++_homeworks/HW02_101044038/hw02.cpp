/**
 * 
 * BIL241 C++ HW02
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
 
#include "hw02.h"


///Person Class Functions
Person::Person(int num) //constructor
{
	if(num > 0)
		age = num;
	else
		age = 1;
}

void Person::setAge(int num) //setter
{
	if(num > 0)
		age = num;
	else
		age = 1;
}

int Person::getAge() { return age; } //getter

bool Person::operator<(Person& per2) //compare
{
	return age < per2.getAge();
}

bool Person::operator>(Person& per2) //compare
{
	return age > per2.getAge();
}

ostream& operator <<(ostream &out, Person &per)
{
	out << per.getAge();	
	return out;
}


///--------------------------------------------------------------

///DayOfYear Class Functions
DayOfYear::DayOfYear(int d) //constructor
{
	if(d > 0)
		day = d;
	else
		day = 1;
}

void DayOfYear::setDay(int d) //setter
{
	if(d > 0)
		day = d;
	else
		day = 1;
}

int DayOfYear::getDay() { return day; } //getter

bool DayOfYear::operator<(DayOfYear& day2) //compare
{
	return day < day2.getDay();
}

bool DayOfYear::operator>(DayOfYear& day2) //compare
{
	return day > day2.getDay();
}

ostream& operator <<(ostream &out, DayOfYear &day)
{
	out << day.getDay();	
	return out;
}


///----------------------------------------------------------------

/*
 * Returned nth element in array
 * 
 * Parameters:
 *             base : array
 *             num : number of elements in array
 *             size : size of array
 *             nth
 *             comparator function
 * 
 * 
 * */
void * return_nth ( const void * base,
					size_t num, 
					size_t size,
					int nth,
					int (* comparator ) (const void *, const void *))
{
	
	 void *nthElem; // nth element
	 int count=0;
	 int comp, flag=1;
	 
	 if( nth > (int)num || nth < 1)
		return NULL;  // invalid nth number
		
	for(int i=0; i < (int)num && flag ; ++i)
	{
		count = 0;
		//finding smallest  elements than nth element
		for(int k=0; k < (int)num ; ++k)
		{
			comp = comparator( base + i*size, base + k*size );
			
			if(comp == 1)
				count++;
		}
		
		if(count == nth-1)// check smallest element numbers
		{
			nthElem=(void*)(base + i*size); // found nth element
			flag=0;
		}
	}	
			
			
	return nthElem;
}

/*
 * Printing araays on screen
 * 
 * */
void printArray(const void * arr, int num, size_t size, char name[NAME_SIZE])
{
	void *p;
	int *numInt;
	double *numDb;
	Person *per;
	DayOfYear *day;
		
	cout << "\n ************************** \n ";
	cout << "\n " << name << " Array: { ";
	for(int i=0; i<num ; ++i)
	{
		if(size == sizeof(int)) // check array type
		{
			p = (int*)(arr+i*size);
			numInt = (int*)p; 
			cout << *numInt  << "  "; 
		}
		else if(size == sizeof(double)) // check array type
		{
			p = (double*)(arr+i*size);
			numDb = (double*)p; 
			cout << *numDb  << "  "; 
		}
		else if(size == sizeof(Person)) // check array type
		{
			p = (Person*)(arr+i*size);
			per = (Person*)p; 
			cout << *per  << "  "; 
		}
		else if(size == sizeof(DayOfYear)) // check array type
		{
			p = (DayOfYear*)(arr+i*size);
			day = (DayOfYear*)p; 
			cout << *day  << "  "; 
		}
	}	
	cout << " } \n\n";
	
}

/*
 * Compare two integer numbers
 * 
 * */
int compInt(const void * num1, const void * num2)
{
	if ( *(int*)num1 <  *(int*)num2 ) 
		return -1;
	else if ( *(int*)num1 >  *(int*)num2 ) 
		return 1;
	
	return 0;
}

/*
 * Compare two double numbers
 * 
 * */
int compDb(const void * num1, const void * num2)
{
	if ( *(double*)num1 <  *(double*)num2 ) 
		return -1;
	else if ( *(double*)num1 >  *(double*)num2 ) 
		return 1;
	
	return 0;
}

/*
 * Compare two Person ages
 * 
 * */
int compPerson(const void * per1, const void * per2)
{	  	
  	if ( *(Person*)per1 <  *(Person*)per2 ) 
		return -1;
	else if ( *(Person*)per1 >  *(Person*)per2 ) 
		return 1;
	
	return 0;
}


/*
 * Compare two DayOfYear days
 * 
 * */
int compDayOfYear(const void * day1, const void * day2)
{
    if ( *(DayOfYear*)day1 <  *(DayOfYear*)day2 ) 
		return -1;
	else if ( *(DayOfYear*)day1 >  *(DayOfYear*)day2 ) 
		return 1;
	
	return 0;
}



////#################################################################
