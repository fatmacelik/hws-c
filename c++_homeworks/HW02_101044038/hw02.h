/**
 * 
 * BIL241 C++ HW02
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
#ifndef HW02_H
#define HW02_H

	#include <iostream>
	#include <cstdlib>

	using namespace std;

	#define SIZE 7
	#define NAME_SIZE 20
	#define CLEAR_SCREEN system("clear")

///*******************************************************************
	// Person Class
	class Person
	{
		public:
		
			Person(int num);  // constructor
			void setAge(int num);  //setter
			int getAge(); // getter
			
			//compares function
			bool operator<(Person& per2);   
			bool operator>(Person& per2);    
			friend ostream& operator <<(ostream &out,Person &per);
			
		private:
			int age; // person's age
	};


	// DayOfYear Class
	class DayOfYear
	{
		public:
		
			DayOfYear(int d);  //constructor 
			void setDay(int d);  //setter
			int getDay(); //getter
			
			//compares functions
			bool operator<(DayOfYear& per2);   
			bool operator>(DayOfYear& per2);    
			
			friend ostream& operator <<(ostream &out,DayOfYear &day);
			
		private:
			int day; // day
	};

///******************************************************************

	// return nth element int array
	void * return_nth ( const void * base,
						size_t num, size_t size,
						int nth,
						int (*comparator ) (const void *, const void *));


	// compare integers
	int compInt(const void * num1, const void * num2);

	//compare double numbers
	int compDb(const void * num1, const void * num2);

	//Compare Persons age
	int compPerson(const void * per1, const void * per2);

	// compare DayOfYear days
	int compDayOfYear(const void * day1, const void * day2);
	
	// printing arrayss
	void printArray(const void * arr, int num, size_t size, char name[NAME_SIZE]);

	// tested integer arrays
	void testInt(int arrInt[SIZE]);

	//tested double array
	void testDb(double arrDb[SIZE]);
	
	// tested Person array
	void testPerson(Person persons[SIZE]);

	// tested DayOfYear array
	void testDayOfYear(DayOfYear days[SIZE]);

#endif

////#########################################################################

