/**
 * 
 * BIL241 C++ HW04
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
#include "hw04.h"


	/*
	 * 
	 *  main functions 
	 * 
	 * */
	int main()
	{	
		char name[FILE_SIZ] = "",
				 file[FILE_SIZ] = "./boards/",
				 text[6] = ".txt" ,
				 temp[FILE_SIZ];
				 
		int N=0;
		
		GameOfLife board;
		
		CLEAR_SCREEN;
		
		cout << "\n Hello! Welcome Game of Life.."
			 << "\n\n Boards file names:\n  "
			 << "\n board0   board1    board2   board3    board4 "
				"\n board5   board6    board7   board8    board9 \n"
			 << "\n Please, enter filename:  ";
		cin >>	 name;
		cout << " Please, enter N step number:  ";
		cin >> temp;
		
		N = checkNum(temp);
		if(N == 0)
		{
			cout << "\n ----------"
					"\n Please check N step number.. Exiting\n"
					"\n ----------\n\n";			
			return 0;
		}
			
		strcat(file,name);
		strcat(file,text);
				
		board.play(file,N);	
			
			
		return 0;
	}
