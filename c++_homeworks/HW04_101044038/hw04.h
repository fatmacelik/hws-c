/**
 * 
 * BIL241 C++ HW04
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
#ifndef HW04_H
#define HW04_H

	#include <iostream>
	#include <cstdio>
	#include <cstdlib>
	#include <vector>
	#include <cstring> // strcat
	
	using namespace std;

	#define CLEAR_SCREEN system("clear")
	#define FILE_SIZ 50

	

	//Cell class
	class Cell
	{
		public:
			//constructors
			Cell();
			Cell(int _x, int _y);
			
			//setters
			void setX(int _x);
			void setY(int _y);
			void set(int _x, int _y);
			void setCell(char c);
			
			//getters
			int getX() const { return x; }
			int getY() const { return y; }
			char getCell() const { return cell; }  
			
		
		private:
			int x;
			int y;	
			char cell;
	};
	

	
	//Game of life class
	class GameOfLife
	{
		public:
		
			//constructors
			GameOfLife();
			GameOfLife(int w, int h, int n);
						
			//setters
			void setWidth(int w);
			void setHeight(int h);
			void setCell(int h, int w, char c);
			
			//getters
			char getCell(int h, int w) const     
			{ 		
				return livingCells[h*width+w].getCell();
			}
			
			int getN() const         { return N; }
			int getWidth() const     { return width; }
			int getHeight() const    { return height; }			
			int numOfLivingCell()    { return numLivingCell; }					
					
			int findNumLivingCell() const;		
					
			//output
			void displayBoard();
			
			//read & write file
			int readFile(FILE* inp);
			int readSizeFromFile(FILE *inp);
			void writeFile(FILE* outp) const;
			
			// play function
			void play(char file[FILE_SIZ], int N);
			
			// invalid charcaters checked
			int checkChars();
			
			// gmae played N step
			void playNstep();
			
			// copy from newBoard to current board
			void copyFromBoardToNewBoard(vector<Cell> newBoard);
			
			// apply board game rules
			void applyRules(vector<Cell>& newBoard);
			
			// find index of neighbors
			void findIndexofNeig( int neigs[8][2], int row, int col) const;
			
			// game finish
			void finishGame() const;
			
			// two board compared
			int  checkSameBoard(vector<Cell> newBoard) const;

			// finding number of neighbors for a cell
			int  findNumOfNeig( int neigs[8][2]) const;
						
			//alive cells of other game joined to current game
			void join( const GameOfLife& otherGame);
		
		
		private:
			vector<Cell> livingCells;
			int width;
			int height;
			int N; // step
			static int numLivingCell; // number of living cells
		
	};
	

	// convert string to int
	int myAtoi(char *arr);
	
	// check char array, if arr have just numbers, return 1 
	int checkNum(char *arr);
	
	// if ch is num, return 1
	int isNum(char ch);
	
	
#endif


////##############################################################


