/**
 * 
 * BIL241 C++ HW04
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
 
#include "hw04.h"


/************************ Cell Class ***************************/
	//constructors
	Cell::Cell(): x(0), y(0)
	{/*empty body*/}
	
	Cell::Cell(int _x, int _y)
	{
		if(_x >= 0)   x = _x;
		else	x = 0;
		
		if( _y >= 0)  y = _y;
		else  y =0;
	}
	
	//setters
	void Cell::setCell(char c)
	{
			cell = c;
	}

	void Cell::setX(int _x) 
	{ 
		if(_x >= 0)   
			x = _x; 
	}
	void Cell::setY(int _y) 
	{ 
		if(_y >= 0)   
			y = _y;
	}
	
	void Cell::set(int _x, int _y)
	{
		if(_x >= 0)  
			x = _x;
				
		if( _y >= 0)  
			y = _y;		
	}
	




/******************** GameOfLife Class ***************************/

	int GameOfLife::numLivingCell = 0;

	//constructors
	GameOfLife::GameOfLife(): width(0), height(0), N(0)
	{/*empty body*/}
	
	GameOfLife::GameOfLife(int w, int h, int n)
	{
		if(w>0)
			width = w;
		
		if(h > 0)
			height = h;	
		
		if(n > 0)
			N = n;	
		
		if( w>0 && h>0)	
			livingCells.resize(w*h);
	}
	
	
	//setters
	void GameOfLife::setWidth(int w)
	{
		if(w>0)
			width = w;
	}
	
	void GameOfLife::setHeight(int h)
	{
		if(h > 0)
			height = h;			
	}
	
	void GameOfLife::setCell(int h, int w, char c)
	{
		livingCells[h*width+w].setCell(c);
		
	}



						

	/* ************************************************************
	 * Operations:
	 * 				printing board on terminal screen
	 * ********************************************************** */
	void GameOfLife::displayBoard() 
	{
		int i,j;
		char blank[] = "         ";
		
		
		numLivingCell = this->findNumLivingCell();
		cout << "\n\n" << blank;
		cout << "Num of living cell: " << this->numOfLivingCell();
		cout << "\n\n" << blank;
		cout << "Size: " << height << " x " << width << endl << blank;		
		for(i=0; i<width+2;++i)	cout << "-";
		
		cout << endl;		
		for(i=0; i<height ; ++i)
		{
			cout << blank << "|";
			for(j=0; j < width ; ++j)
				cout << this->getCell(i,j);
			
			cout << "|" << endl;
		}	
		cout << blank;
		for(i=0; i<width+2;++i)
			cout << "-";
			
		cout << "\n\n **************************************************\n";
	}
		
		
	/* ************************************************************
	 * Operations:
	 * 				invalid charcater changed as blank character
	 * ********************************************************** */
	int GameOfLife::checkChars()
	{
		int i,k;
		
		for(i=0; i<height ; i++)
		{	for(k=0 ; k<width ; ++k)
			{
				if(this->getCell(i,k) != 'X' && this->getCell(i,k) != '_')
				{				
					return 0;
				}
				
				if(this->getCell(i,k) == '_')
				     this->setCell(i,k,' ');
			}	}	
			
		return 1;	
	}
	
	
	/* ************************************************************
	 * Operations:
	 * 				read a board from given file name by user
	 *              store board vector
	 * ********************************************************** */
	int GameOfLife::readFile(FILE* inp) 
	{		
		int i,j;
		char *temp;
		
		temp = (char*)malloc((width+2) * sizeof(char));					
		for(i=0; i<height;++i)
		{
			fscanf(inp,"%s",temp);
			if( temp[0] == '\0')
				return 0;
				
			for(j=0; j<width && temp[0] != '\0'; ++j)
			{				
				this->setCell(i,j, temp[j]);
			}
			
			temp[0] =  '\0';								
		}				
		free(temp);	
		
		return this->checkChars();						
	}
	
	
	/* ************************************************************
	 * Operations:
	 * 				printing result board on file
	 * ********************************************************** */
	void GameOfLife::writeFile(FILE* outp) const
	{
		int i,k;
		
		fprintf(outp,"Num Of Living Cell: %d \n",numLivingCell);
		fprintf(outp,"Board size: %d x %d\n",height,width);
		for(i=0; i< height; ++i){
			for(k=0; k<width; ++k)
			{
				if(this->livingCells[i*width+k].getCell() != 'X')
					fprintf(outp,"_"); // blank
				else
					fprintf(outp,"X");
			}			
			fprintf(outp,"\n");
		}
	}
	
	
	
	
	
	/* ************************************************************
	 * Operations:
	 * 				opened/closed board file				
	 * 				call readBoardFromFile function
	 * 				call printBoard function				
	 * ********************************************************** */
	void GameOfLife::play(char file[FILE_SIZ], int n)
	{
		FILE* inp;    // board file
		int status;		
		
		inp = fopen(file,"r"); // opened file
		if(inp != NULL){			

			if( this->readSizeFromFile(inp) > 0)
			{	
				this->N = n;				
				status = this->readFile(inp); //read board and size from file	
				fclose(inp);	// closed file
				
				if( status == 1){							
					// play game N times
					this->playNstep();
					cout << "\n Exiting!"
						 << "\n\n **************************************************\n\n";					
				}
				else
					cout << "\n ------------ \n\n Please check board file..  Exiting..\n "
				            "\n ------------ \n\n" ;													
			}
			else{
				cout << "\n ------------ \n\n Board size is invalid.. Please check size!  Exiting..\n "
				        "\n ------------ \n\n" ;								
				fclose(inp);
				}			
		}else
			cout << "\n Not opened " << file << " file!\n Exiting..\n\n" ;	
	}



	/* ************************************************************
	 * Operations:
	 * 				other GameOfLife joined  in current GameOfLife 
	 * ********************************************************** */
	void GameOfLife::join( const GameOfLife& otherGame)
	{
		int i,j;
		if( this->width == otherGame.getWidth()  &&
		    this->height == otherGame.getHeight())
		{				
			for(i=0; i<height ; ++i)
			{
				for(j=0; j<width ; ++j)
				{
					if( otherGame.livingCells[i*width+j].getCell() == 'X')
						this->setCell(i,j,'X');	
				}
			}// for end			
		}//if end	
	}

	/* ************************************************************
	 * Operations:
	 * 				copy a board into a new board
	 * ********************************************************** */
	void GameOfLife::copyFromBoardToNewBoard(vector<Cell> newBoard)
	{
		int i,j;
					
		for(i=0; i<height;++i)
		{
			for(j=0; j<width ; ++j)				
				this->setCell(i,j,newBoard[i*width+j].getCell());
		}				
	}


	/* ************************************************************
	 * Operations:
	 *              stored index of all neighbors into neigs[8][2] array
	 *              there are 8 neighbors
	 * ********************************************************** */
	void GameOfLife::findIndexofNeig( int neigs[8][2], int row, int col ) const
	{
		int k;
		int temp[8][2] = {  {row-1,col-1}, {row-1,col}, {row-1,col+1},
							{row,col-1},                {row,col+1},
							{row+1,col-1}, {row+1,col}, {row+1,col+1}   
						};			
		
		for(k=0; k<8 ;++k)
		{			
			neigs[k][0]= temp[k][0];
			neigs[k][1]= temp[k][1];		
		}					
	}
	
	
	/* ************************************************************
	 * Operations:
	 * 				finding all neighbors of a cell,
	 *              return count of neighbors
	 * ********************************************************** */
	int GameOfLife::findNumOfNeig( int neigs[8][2]) const
	{
		int count=0;
		int k,row,col; // index
				
		for(k=0; k<8; ++k)
		{
			row = neigs[k][0];
			col = neigs[k][1];
			
			if(row >=0 && row <height && col >=0 && col < width)
			{
				if( this->livingCells[row*width+col].getCell()== 'X')
					count++;			
			} 
		}
		
		return count;
	}



	/* ************************************************************
	 * Operations:
	 * 				 game of life rules are applied in board and
	 *               stored new board
	 * ********************************************************** */
	void GameOfLife::applyRules(vector<Cell>& newBoard)
	{
		int i,k;
		int neigs[8][2]; // array is indexs of neighbors
		int countNeig=0;
		
		for(i=0; i<height ; i++)
		{
			for(k=0; k<width; ++k){
				this->findIndexofNeig(neigs, i, k);
				countNeig = this->findNumOfNeig(neigs);
		
				if( (countNeig == 2  || countNeig == 3) && this->livingCells[i*width+k].getCell() == 'X')					
					newBoard[i*width+k].setCell('X');				
			
				else if(  countNeig == 3 && this->livingCells[i*width+k].getCell() == ' ')
					newBoard[i*width+k].setCell('X');				
			
				else 
					newBoard[i*width+k].setCell(' ');				
			}
		}//end for	
	}
	
	
	

	/* ************************************************************
	 * Operations:
	 * 				comparing two board
	 * ********************************************************** */
	int  GameOfLife::checkSameBoard( vector<Cell> newBoard) const
	{
		int i,j;
		
		for(i=0; i<height; ++i)
		{
			for(j=0; j<width; ++j)
			{
				if( this->livingCells[i*width+j].getCell()
					!= 
					newBoard[i*width+j].getCell())
					
					return 0; // not same two board
			}	
		}
		
		return 1;	// same two board
	}


	/* ************************************************************
	 * Operations:
	 * 	 			find number of living cell
	 * ********************************************************** */
	int GameOfLife::findNumLivingCell() const
	{
		int i,j, count=0;
		
		for(i=0; i<height ; ++i)
		{
			for(j=0; j<width ; ++j)
			{
				if( this->livingCells[i*width+j].getCell() == 'X')
					count++;	
			}
		}// for end	
		
		return count;
	}


	/* ************************************************************
	 * Operations:
	 * 	 			call copyFromBoardToNewBoard function
	 * 				call applyRules function
	 * 
	 * 				user playing game of life
	 * 	            user enter '\n' character and go new step until N step
	 *              if step is N, ask user to new file name for printing new board 
	 * ********************************************************** */
	void GameOfLife::playNstep()
	{
		char ch;
		int status1=0, status2=0,flag=1,k;

		GameOfLife  newBoard(width, height, N),
					preBoard(width, height, N);
		
		newBoard.copyFromBoardToNewBoard(this->livingCells);
		preBoard.copyFromBoardToNewBoard(this->livingCells);

		cin.get(ch); 
		this->displayBoard();
		for(k=1; k<=N && flag; ++k)
		{		
			cout << "\n Step" << k << " - Please enter to continue..";
			cin.get(ch); //  take an '\n' enter character		
			this->applyRules(newBoard.livingCells);	
			newBoard.displayBoard();				

			status1 = this->checkSameBoard(newBoard.livingCells);				
			if(k>2)
				status2 = newBoard.checkSameBoard(preBoard.livingCells); //check oscillator
			if(status1 == 1 || status2 == 1)		
			{
				flag = 0;
				cout << "\n " << k << ". step : Game finished...";	
				if(status1 == 1)				
					cout << "\n Same Board is repeated...";
				if(status2 == 1)
					cout << "\n This Board is Oscillators! \n";
								
				this->copyFromBoardToNewBoard(newBoard.livingCells);	
				this->finishGame();					     
			}
			else
			{				
				preBoard.copyFromBoardToNewBoard(this->livingCells);						
				this->copyFromBoardToNewBoard(newBoard.livingCells);
			}				
		}
		
		if(flag){
			this->copyFromBoardToNewBoard(newBoard.livingCells);											
			cout << "\n In step " << this->N << ",  Game finished...\n";
			this->finishGame();
		}
	}


	/* ************************************************************
	 * Operations:
	 * 				Print finish message
	 * 				ask user file name for new board 
	 * ********************************************************** */
	void GameOfLife::finishGame() const
	{
		FILE *outp;				
			char name[FILE_SIZ] = "",
			 file[FILE_SIZ] = "./newBoards/";
			 			 
		cout << "\n Please, enter a file name to print new board.."	
			 << "\n Enter file name:  ";
		cin >> name;	
		
		strcat(file,name);
								
		outp = fopen(file,"w");	//opened file
		
		cout << "\n Created new board file: " << file << endl; 				
		this->writeFile(outp);
		
		fclose(outp);	// closed file	
	}


	/* ************************************************************
	 * Operations:
	 * 				read board size from file
	 * ********************************************************** */
	int GameOfLife::readSizeFromFile(FILE *inp)
	{
		char temp[FILE_SIZ]="";
				
		//height
		fscanf(inp,"%s",temp);		
		if(temp[0] == '\0')   return 0;		
		height = checkNum(temp);
		if(height < 1)
			return 0;
			
		//width
		fscanf(inp,"%s",temp);		
		if(temp[0] == '\0')   return 0;		
		width = checkNum(temp);
		if(width < 1)
			return 0;
			
		this->livingCells.resize(width*height);
		
		return 1;
	}
	
	
	
	
	
	
	
	/* ************************************************************
	 * Operations:
	 * 	 			if ch is num, return 1, other return 0
	 * ********************************************************** */
	int isNum(char ch)
	{
		char nums[12] = "0123456789";
		int i;
		
		for(i=0; nums[i] != '\0'; ++i){
			if(ch == nums[i])
				return 1;
		}	
		return 0;
	}
	
	/* ************************************************************
	 * Operations:
	 * 	 			cnvert string to int
	 * ********************************************************** */	
	int myAtoi(char *arr)
	{
		int i,num=0;
		
		for(i=0; arr[i] != '\0' ; ++i)
			num = (num *10) + (arr[i] - '0');
		
		return num;
	}


	/* ************************************************************
	 * Operations:
	 * 	 			if char array  have just  number characters,
	 *                                   return 1, other 0
	 * ********************************************************** */
	int checkNum(char *arr)
	{
		int i, status,size=0;
		
		for(i=0; arr[i] != '\0' ; ++i)
		{
			status = isNum(arr[i]);
			if(status == 0)
				return 0;
		}
		
		size = myAtoi(arr);
		
		return size;
	}





//#################################################################
