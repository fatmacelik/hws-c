/**
 * 
 * BIL241 C++ HW03
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
 
#include "hw03.h"


	/*
	 * Constructors 
	 * 
	 * */
	Vect3D::Vect3D(): _x(0.0), _y(0.0), _z(0.0) 
	{// empty body
	}

	Vect3D::Vect3D(double x): _x(x), _y(0.0), _z(0.0)
	{// empty body
	}
	 
	Vect3D::Vect3D(double x, double y): _x(x), _y(y), _z(0.0)
	{// empty body
	}
	
	Vect3D::Vect3D(double x, double y, double z): _x(x), _y(y), _z(z)
	{// empty body
	}


	/* 
	 * Setters
	 * */
	void Vect3D::set( double x, double y, double z)
	{
		_x = x;
		_y = y;
		_z = z;				
	}
	void Vect3D::setX(double x)   { _x = x; }
	void Vect3D::setY(double y)   { _y = y; }
	void Vect3D::setZ(double z)   { _z = z; }


	//getters
	double Vect3D::getX() const   { return _x; }
	double Vect3D::getY() const   { return _y; }
	double Vect3D::getZ() const   { return _z; }



	/*
	 *  take vectors elements from terminal
	 * */
	void Vect3D::input()
	{
		cout << " \n Please enter vectors elements X, Y, Z :  ";
		
		cin >> _x >> _y >> _z;
	}
	
	/*
	 *  print on screen vector elements
	 * */
	void Vect3D::output() const
	{
		cout << "  x( "  
		     <<  _x  <<  " )  y( " 
			 <<  _y  <<  " )  z( "
			 <<  _z  <<  " ) \n";		
	}



	/*
	 * Calculate dot product of this vector and another vector
	 * 
	 * sum = x1*x2 + y1*y2 + z*z2
	 * 
	 * 
	 * */
	double Vect3D::dotProduct( const Vect3D& vec) const
	{
		double result;
		
		result = _x * vec.getX()  +  _y * vec.getY()  + _z * vec.getZ();
		
		return result;
	}
		
		
	/*
	 * calculate two vectors product and returns new vector
	 *  
	 * cross product
	 * http://www.mathsisfun.com/algebra/vectors-cross-product.html
	 * 
	 *  a * b
	 * 
	 * 	cx = ay * bz   −   az * by 
	 *	cy = az * bx   −   ax * bz 
	 *	cz = ax * by   −   ay * bx  
	 *
	 * 
	 * */
	Vect3D Vect3D::vectorProduct( const Vect3D& vec) const
	{
		double x,y,z;

		x = _y * vec.getZ() - _z * vec.getY();
		
		y = _z * vec.getX() - _x * vec.getZ();
		
		z = _x * vec.getY() - _y * vec.getX();	
		
		return Vect3D(x,y,z);	
	}



	/*
	 * calculate magnitude of vector
	 * 
	 * */
	double Vect3D::magnitute() const
	{
		double magn; // magnitude
			
		magn =  pow(_x,2) + pow(_y,2) + pow(_z,2);
		magn = sqrt(magn);
		
		return magn;		
	}
		


//#################################################################
