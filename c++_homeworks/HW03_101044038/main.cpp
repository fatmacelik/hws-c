/**
 * 
 * BIL241 C++ HW03
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
#include "hw03.h"


// test functions
// call by value
void test1(Vect3D vec1, Vect3D vec2, Vect3D vec3, Vect3D vec4);

// call by reference
void test2(Vect3D& vec5, Vect3D& vec6, Vect3D& vec7);



/*
 * 
 *  main functions / call test functions
 * 
 * */
int main()
{
	Vect3D vec1, vec2(3.5), vec3(4,-2), vec4(7.8,-5), 
	       vec5(12.4,5.2,-1.4), vec6(4.2,1.7,10.5), vec7(1.2,2.5,-2.4);
	 
	 
	CLEAR_SCREEN;
	     	     
	cout << "\n ************  VECTORS    *************";
	cout << "\n vector1: " ; vec1.output();
	cout << "\n vector2: " ; vec2.output();
	cout << "\n vector3: " ; vec3.output();
	cout << "\n vector4: " ; vec4.output();
	cout << "\n vector5: " ; vec5.output();
	cout << "\n vector6: " ; vec6.output();
	cout << "\n vector7: " ; vec7.output();	    
	cout << "\n *************************************** \n\n";
     
	  
	 // call test functions     
	 test1(vec1, vec2 , vec3 , vec4);
	 test2( vec5, vec6, vec7);  
	 
	 cout << "\n\n ---------------";
	 cout << "\n Result of Call by Value: "   
	      << "\n vector 1: "; vec1.output();
	 cout << " vector 2: "; vec2.output();       
	 cout << " vector 3: "; vec3.output();
	 cout << " vector 4: "; vec4.output();
	 
	 
	 cout << "\n ---------------";
	 cout << "\n Result of Call by Reference: "   
	      << "\n vector 5: "; vec5.output();
	 cout << " vector 6: "; vec6.output();       
	 cout << " vector 7: "; vec7.output();
	 
	 
	 cout << "\n\n *******************  END  ********************** \n\n";
	 
	return 0;
}

/*
 * 
 *  test 1 function / call by value
 * */
void test1(Vect3D vec1, Vect3D vec2, Vect3D vec3, Vect3D vec4)
{
	Vect3D newVec;
	
	//setters
	cout << "\n ---------------";
	cout << "\n Set vector1: "; vec1.set(1,2,3); vec1.output();
	cout << "\n Set vector2: "; vec2.set(-10,-15,-20); vec2.output();

	// dot product
	cout << "\n ---------------";
	cout << "\n dot product( vec3 . vec2) =  " << vec3.dotProduct(vec2);
	cout << "\n dot product( vec1 . vec4) =  " << vec1.dotProduct(vec4);
	cout << "\n dot product( vec2 . vec1) =  " << vec2.dotProduct(vec1);

	// vector product
	cout << "\n ---------------";
	cout << "\n\n vector product( vec3 x vec2) =  ";
	newVec =  vec3.vectorProduct(vec2); newVec.output();
	
	cout << " vector product( vec1 x vec4) =  ";
	newVec =  vec1.vectorProduct(vec4); newVec.output();

	cout << " vector product( vec2 x vec1) =  ";
	newVec =  vec2.vectorProduct(vec1); newVec.output();


	//magnitude
	cout << "\n ---------------";
	cout << "\n Magnitudes : ";
	cout << "\n vector1 magnitude =  " << vec1.magnitute();
	cout << "\n vector2 magnitude =  " << vec2.magnitute();
	cout << "\n vector3 magnitude =  " << vec3.magnitute();
	cout << "\n vector4 magnitude =  " << vec4.magnitute();
}



/*
 * test2 function call by reference
 * 
 * */
void test2(Vect3D& vec5, Vect3D& vec6, Vect3D& vec7)
{
	//setters
	cout << "\n ---------------";
	cout << "\n Set vector5: "; vec5.set(12,6,-3); vec5.output();
	cout << "\n Set vector6: "; vec6.set(2.7,5,10); vec6.output();
	cout << "\n Set vector7: "; vec7.set(-1,5.2,2.4); vec7.output();

	//setters
	vec5.setX(3); 
	vec5.setY(6); 
	vec5.setZ(-2); 
	cout << "\n Set vector5: ";  vec5.output();
	
	//magnitude
	cout << "\n ---------------";
	cout << "\n Magnitudes : ";
	cout << "\n vector5 magnitude =  " << vec5.magnitute();
	cout << "\n vector6 magnitude =  " << vec6.magnitute();
	cout << "\n vector7 magnitude =  " << vec7.magnitute();
	
	// input
	cout << "\n ---------------";
	cout << "\n Input functions: \n Vector6: ";
	vec6.input();
}



//##################################################################
