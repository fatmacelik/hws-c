/**
 * 
 * BIL241 C++ HW03
 * 
 *  FATMA CELİK
 *  
 *  101044038
 * 
 * */
 
 
#ifndef HW03_H
#define HW03_H

	#include <iostream>
	#include <cstdlib>
	#include <cmath>//pow, sqrt
	
	using namespace std;

	#define CLEAR_SCREEN system("clear")
	
	//VECT3D class
	class Vect3D
	{
		public:
			//constructors
			Vect3D(); // 			
			Vect3D(double x);
			Vect3D(double x, double y);
			Vect3D(double x, double y, double z);
			
			//setters
			void set( double x, double y, double z);
			void setX(double x);
			void setY(double y);
			void setZ(double z);
	
			
			//getters
			double getX() const;
			double getY() const;
			double getZ() const;
			
			//product
			double dotProduct( const Vect3D& vec) const;
			Vect3D vectorProduct( const Vect3D& vec) const;
			
			double magnitute() const;
				
			//input output
			void input();
			void output() const;
			
		private:
			double _x;
			double _y;
			double _z;					
	};
	

#endif


////##############################################################


