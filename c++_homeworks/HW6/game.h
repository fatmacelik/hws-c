/*
 * 
 * BIL241 HW06
 * 
 * FATMA CELIK
 *  101044038
 * 
 * 
 * */
#ifndef GAME_H
#define	GAME_H

#include <iostream>
#include <fstream>
#include <cstdlib>//exit()
#include <istream>
#include "cell.h"

#define DEAD '.'
#define ALIVE 'x'

using namespace std;

/*
 *
 * 	class GameOfLife prototype
 *
 */
 namespace FatmaCelik
 {
			 
		class GameOfLife {
			public:
				GameOfLife();
				GameOfLife(int);
				GameOfLife(int, int);
				GameOfLife( const GameOfLife&);
				~GameOfLife();


				// mutators
				void set(int, int);
				void setWidth(int);
				void setHeight(int);


				//member functions
				void read(char[]);
				static int alive();

				//accessors
				int getWidth()const;
				int getHeight()const;
				
				
				int getSize()const;
				void push_back(const Cell& );
				void push_game(const GameOfLife& );
				void pop_game();
				void erase(int);


				const GameOfLife operator[](int);
				Cell operator[](int)const;

				Cell operator()(int, int)const;
				const GameOfLife operator+=(const GameOfLife&);
				const GameOfLife operator=(const GameOfLife&);

				//pre pos decrement operators
				const GameOfLife& operator++();
				GameOfLife operator++(int);

				//pre pos increment operators
				const GameOfLife& operator--();
				GameOfLife operator--(int);
				// stream insertion operator

				friend ostream& operator <<(ostream&, const GameOfLife&);
				friend const GameOfLife operator+(const GameOfLife&, const Cell&);
				friend const GameOfLife operator-(const GameOfLife&, const Cell&);
				friend const GameOfLife operator+(const GameOfLife&, const GameOfLife&);
				friend const GameOfLife operator-(const GameOfLife&, const GameOfLife&);


			private:
				Cell *livingCell; // cell array
				GameOfLife *history; // gameoflife array
				static int aliveCell;
				int width;
				int height;
				int size; // cell array size
				
				//private functions
				bool isCellAlive(int, int)const;
				bool isCellAlive(Cell)const;
				bool survive(int, int);
				void extend();
				void extendToLeft();
				void extendToRigth();
				void extendToUp();
				void extendToDown();
				void evaluate(char*, int);
				int countNeighbours(int, int);

		};

}
#endif
