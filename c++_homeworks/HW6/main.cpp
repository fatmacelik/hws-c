/*
 * 
 * BIL241 HW06
 * 
 * FATMA CELIK
 *  101044038
 * 
 * 
 * */


#include "game.h"

#define FILE_SIZ 50
#define CLEAR_SCREEN system("clear")


using namespace FatmaCelik;


void test( GameOfLife& board1, GameOfLife& board2, GameOfLife& board3, 
				   GameOfLife& board4,  GameOfLife& board5);
/*
 * 
 *  main functions 
 * 
 * */
int main()
{	
	char file1[FILE_SIZ] = "./boards/board1.txt",
		 file2[FILE_SIZ] = "./boards/board2.txt",
		 file3[FILE_SIZ] = "./boards/board3.txt",
		 file4[FILE_SIZ] = "./boards/board4.txt",
		 file5[FILE_SIZ] = "./boards/board5.txt";		 			 
			
	GameOfLife board1, board2, board3, board4, board5;
	
	CLEAR_SCREEN;
			
	// fill boards from file and set N step random		
	board1.read(file1);					
	board2.read(file2);	
	board3.read(file3);	
	board4.read(file4);	
	board5.read(file5);	

	// test  ostream operator overloading
	cout << "Board1:\n" << board1 
		 << "\nBoard2:\n" << board2 
		 << "\nBoard3:\n" << board3 
		 << "\nBoard4:\n" << board4 
		 << "\nBoard5:\n" << board5;

	test(board1, board2, board3, board4, board5);
	
	return 0;
}


/*
 *  test of gameoflife and cell objects with operator overloading methods
 * 
 * 
 * */
void test( GameOfLife& board1, GameOfLife& board2, GameOfLife& board3, 
				   GameOfLife& board4,  GameOfLife& board5)

{
	GameOfLife new1, new2; // test objects
	Cell c1(4,5), c2(2,3), c3(10,2);  // test cell objects
	
	
	// test operator += overloading
	board1 += board2;
	board3 += board4;
	board5 += board4;
	
	// print new boards on screen
	// test  ostream operator overloading
	cout << "\n New Board1: \n" << board1 
		 << "\n New Board2: \n" << board2 
		 << "\n New Board3: \n" << board3 
		 << "\n New Board4: \n" << board4 
		 << "\n New Board5: \n" << board5;
	
	board1 += board3;
	board2 += board5;
	
	// test operator + , - overloading arguman cell object
	new1 = board3 + c1;
	new2 = board4 - c2;	
	cout << "\n New 1: \n" << new1 
		 << "\n New 2: \n" << new2;
		 
		 
	// test operator + ,  - arguman Gameoflife object
	new1 = board4 + board3;
	new2 = board1 + board2;
	cout << "\n New 3: \n" << new1 
		 << "\n New 4: \n" << new2;
	
	new1 = board2 - board1;
	new2 = board3 - board4;
	cout << "\n New Board1: \n" << new1 
		 << "\n New Board2: \n" << new2;
	


	//// cell test
	cout << "\n ******** CELL TEST **************\n";
	// test of operator index overloading ,  []  () 
	cout << " Cell board3[7]  : "  << board3[7] << endl;
	cout << " Cell board5[12] : "  << board5[12] << endl;
	cout << " Cell board4[10]  : "  << board4[10] << endl;
	cout << " Cell board1(3,4) : "  << board1(3,4) << endl;
	cout << " Cell board3(2,5) : "  << board3(2,5) << endl;
	
	
	//******** CELL ++ , -- TESTS **************
	c1++;
	++c2;
	--c3;
	c1--;
	
	
	
	cout << "\n ******** CELL COMPARE TESTS **************\n";
	if( c1 > c2)  cout << "c1 > c2  : true\n";
	else          cout << "c1 > c2  : false\n";
	
	if( c1 < c3)  cout << "c1 < c3  : true\n";
	else          cout << "c1 < c3  : false\n";
	
	if( c3 == c2) cout << "c3 == c2 : true\n";
	else          cout << "c3 == c2 : false\n";
	
	if( c1 != c2) cout << "c1 != c2 : true\n";
	else          cout << "c1 != c2 : false\n";
	
	if( c1 >= c2) cout << "c1 >= c2 : true\n";
	else          cout << "c1 >= c2 : false\n";
	
	if( c3 <= c1) cout << "c3 <= c1 : true\n";
	else          cout << "c3 <= c1 : false\n";	
	
	cout << endl << endl;
}



