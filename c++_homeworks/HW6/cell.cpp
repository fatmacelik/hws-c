
/*
 * 
 * BIL241 HW06
 * 
 * FATMA CELIK
 *  101044038
 * 
 * 
 * */
#include "cell.h"



namespace FatmaCelik
{
			
		/*
		 *
		 *
		 *
		 * 	class Cell implementations
		 *
		 *
		 *
		 *
		 */

		/**
		 * no parameter constructor
		 *
		 */
		Cell::Cell()
			:x(0),y(0)
		{/*Body intentionally empty.*/}


		/**
		 * one parameter constructor
		 *
		  *		x can't be lower than 0
		 */
		Cell::Cell(int inputX)
			:x(inputX),y(0)
		{/*Body intentionally empty.*/}

		/**
		 * two parameter constructor
		 *
		 *		x and  can't be lower than 0
		 */
		Cell::Cell(int inputX, int inputY)
			:x(inputX),y(inputY)
		{/*Body intentionally empty.*/}

		/**
		 *  set():	takes two integers, check validity of the inputs,
		 * 			and assign them to member variables x and y
		 *
		 *		inputX:	input for member variable x
		 *		inputY:	input for member variable y
		 *
		 *		x's value can't be lower than 0
		 *		y's value can't be lower than 0
		 *
		 */
		void Cell::set(int inputX, int inputY){
				x = inputX;
				y = inputY;
		}


		/**
		 *  setX():	takes an integer, check validity of the input,
		 * 			and assign it to member variable x
		 *
		 *		input:	input for member variable x
		 *
		 *		x's value can't be lower than 0
		 */
		void Cell::setX(int input){
				x = input;
		}

		/**
		 *  setY():	takes an integer, check validity of the input,
		 * 			and assign it to member variable y
		 *
		 *		input:	input for member variable y
		 *
		 *		y's value can't be lower than 0
		 */
		void Cell::setY(int input){
				y = input;
		}

		/**
		 * 	getX():	accessor of member variable x
		 *
		 */
		int Cell::getX()const{
			return x;
		}

		/**
		 * 	getY():	accessor of member variable y
		 *
		 */
		int Cell::getY()const{
			return y;
		}



		/*  ################## new method ##############################*/
		/*
		 * assignment operator overloading
		 * 
		 * */
		Cell& Cell::operator=(const Cell& arg)
		{
			x = arg.x;
			y = arg.y;
			
			return *this;
		}

		/*  #########################################################*/

		/**
		 *	operator<:	takes another const Cell reference. compare two objects
		 *
		 *				if first object is smaller than arg returns true
		 *				else returns false
		 *
		 */
		bool Cell::operator<(const Cell& arg)const{
			return (getY() <= arg.getY() && getX() < arg.getX());
		}
		/**
		 *	operator>:	takes another const Cell reference. compare two objects
		 *
		 *				if first object is greater than arg returns true
		 *				else returns false
		 *
		 */
		bool Cell::operator>(const Cell& arg)const{
			return (getY() >= arg.getY() && getX() > arg.getX());
		}

		/**
		 *	operator<=:	takes another const Cell reference. compare two objects
		 *
		 *				if first object is smaller than arg object or objects are equal returns true
		 *				else returns false
		 *
		 */
		bool Cell::operator<=(const Cell& arg)const{
			return (getY() <= arg.getY() && getX() <= arg.getX());
		}
		/**
		 *	operator>=:	takes another const Cell reference. compare two objects
		 *
		 *				if first object is greater than arg object or objects are equal returns true
		 *				else returns false
		 *
		 */
		bool Cell::operator>=(const Cell& arg)const{
			return (getY() >= arg.getY() && getX() >= arg.getX());
		}
		/**
		 *	operator!=:	takes another const Cell reference. compare two objects
		 *
		 *				if objects are equal returns false
		 *				else returns true
		 *
		 */
		bool Cell::operator!=(const Cell& arg)const{
			return !(getY() == arg.getY() && getX() == arg.getX());
		}

		/**
		 *	operator==:	takes another const Cell reference. compare two objects
		 *
		 *				if objects are equal returns true
		 *				else returns false
		 *
		 */
		bool Cell::operator==(const Cell& arg)const{
			return (getY() == arg.getY() && getX() == arg.getX());
		}
		/**
		 *	prefix operator++ implementation
		 *
		 *		adds 1 to x and y both
		 */
		const Cell Cell::operator++(){
			set(getX() + 1, getY() + 1);

			return *this;
		}

		/**
		 *	postfix operator++ implementation
		 *
		 *		adds 1 to x and y both
		 */
		const Cell Cell::operator++(int){
			Cell temp(getX(), getY());

			set(getX()+1, getY() + 1);

			return temp;
		}

		/**
		 *	prefix operator-- implementation
		 *
		 *	subtracts 1 from x and y
		 *	if x or y gets a value under 0 terminates the program
		 */
		const Cell Cell::operator--(){
			set(getX() - 1, getY() - 1);

			return Cell(getX(), getY());
		}

		/**
		 *	postfix operator-- implementation
		 *
		 *	subtracts 1 from x and y
		 *	if x or y gets a value under 0 terminates the program
		 */
		const Cell Cell::operator--(int){
			Cell temp(getX(), getY());

			set(getX() - 1, getY() - 1);

			return temp;
		}
		/**
		 *	opeartor<<:	stream insertion operator takes a sream an a const
		 *				Cell reference. write info about Cell object to out stream
		 *
		 */
		ostream& operator<<(ostream& out, const Cell& arg){
			out<< "( "<< arg.getY() <<", "<< arg.getX()<<")";
			return out;
		}

		/**
		 *	operator>>:	stream extraction operator takes a const stream and a Cell
		 *				object reference, get two int input assign them to x and y
		 *
		 *		inp:	istream reference. operator get input from this stream
		 *		arg:	oparetor put inputs to this object's members
		 *
		 *		operator get input for x first from stream
		 */
		istream& operator>> (istream& inp, Cell& arg){
			inp >> arg.x >> arg.y;

			return inp;
		}


} // namespace end
