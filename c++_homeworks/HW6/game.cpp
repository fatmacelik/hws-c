/*
 * 
 * BIL241 HW06
 * 
 * FATMA CELIK
 *  101044038
 * 
 * 
 * */
#include "game.h"

/*
 *
 *
 *
 * 	class GameOfLife implementations
 *
 *
 *
 */


namespace FatmaCelik
{
		
	/**
	 *	member static int variable aliveCell
	 */
	int GameOfLife::aliveCell = 0;

	/**
	 * no argument constructor
	 *
	 */
	GameOfLife::GameOfLife()
		:width(0), height(0), size(0)
	{/*Body intentionally empty.*/}

	/**
	 * one argument constructor
	 *
	 *		width can't be lower than 0
	 */
	GameOfLife::GameOfLife(int inpWidth)
		:width(inpWidth), height(0), size(0)
	{
		if( width < 0 ){
			exit(0);
		}
	}

	/* ######################### new methods ########################## */

	/*
	 * Copy
	 * Constructor
	 * */
	GameOfLife::GameOfLife( const GameOfLife& arg) // copy constructor
								   :width(arg.width), height(arg.height)
	{
		if( size != arg.size)
		{
			size = arg.size;
			livingCell = new Cell[size];	
		}	
		
		if( aliveCell != arg.aliveCell)
		{	
			aliveCell = arg.aliveCell;
			history = new GameOfLife[aliveCell];
		}
		
		width = arg.width;
		height = arg.height;
		for(int i=0; i<size; ++i)
			livingCell[i] = arg.livingCell[i];
			
		for(int i=0; i<aliveCell; ++i)
			history[i] = arg.history[i];			
	}

	/*
	 * 
	 * asignment operator overloading
	 * 
	 * 
	 * */
	const GameOfLife GameOfLife::operator=(const GameOfLife& arg)
	{
		
		if( size != arg.size)
		{
			size = arg.size;
			livingCell = new Cell[size];	
		}	
		
		if( aliveCell != arg.aliveCell)
		{	
			aliveCell = arg.aliveCell;
			history = new GameOfLife[aliveCell];
		}
		
		width = arg.width;
		height = arg.height;
		for(int i=0; i<size; ++i)
			livingCell[i] = arg.livingCell[i];
			
		for(int i=0; i<aliveCell; ++i)
			history[i] = arg.history[i];	
		
		return *this;
	}



	/*
	 * Destructor
	 * 
	 * 
	 * */
	GameOfLife::~GameOfLife()
	{
		
		if( livingCell != NULL && size!= 0)
			delete [] livingCell;
		
		if( history != NULL && aliveCell!=0)	
			delete [] history;	
	}




	/*
	 * 
	 * add Cell element into livinngcell array
	 * 
	 * */
	void GameOfLife::push_back(const Cell& arg)
	{
		Cell *temp;
		int i;
		
		temp = new Cell[size+1];
		
		for( i=0; i<size; ++i)
			temp[i] = livingCell[i];
		
		temp[i]=arg;
		
		size++;
		livingCell = new Cell[size];
		
		//height = size/width;
		
		for(i=0; i<size; ++i)
			livingCell[i] = temp[i];
			
		delete [] temp;
	}


	/*
	 * add element into history array
	 * 
	 * */
	void GameOfLife::push_game(const GameOfLife& obj)
	{
		GameOfLife *temp;
		int i;
		temp = new GameOfLife[aliveCell+1];
		
		for( i=0; i<aliveCell ; ++i)
			temp[i] = history[i];
		
		temp[i] = obj;
		
		aliveCell += 1;
		history = new GameOfLife[aliveCell];
		
		for( i=0; i<aliveCell-1 ; ++i)
			history[i] = temp[i];
			
		
	}



	/*
	 * 
	 * remove last element of history array
	 * 
	*/
	void GameOfLife::pop_game()
	{
		GameOfLife *temp;
		
		temp = new GameOfLife[aliveCell-1];
		aliveCell = aliveCell-1;
		
		for(int i=0; i <aliveCell ; ++i)
			temp[i] = history[i];
			
		history = new GameOfLife[aliveCell];
		
		for(int i=0; i<aliveCell ; ++i)
			history[i] = temp[i];
			
		//delete [] temp;	
	}


	/**
	 * in index, remove element from array
	 * 
	 * 
	 * */
	void GameOfLife::erase(int index)
	{
		Cell *temp;
		
		temp = new Cell[size];
		
		for(int i=0; i < index ; ++i)
			temp[i] = livingCell[i];
			
		for(int k=index+1; k< size-1 ; ++k)
			temp[k] = livingCell[k];
				
		livingCell = new Cell[size];
		
		for(int k=0; k< size ; ++k)
			livingCell[k] = temp[k];
			 
		delete[] temp;	
	}


	/* #############################################################  */

	/**
	 * two argument constructor
	 *
	 *		height and width can't be lower than 0
	 */
	GameOfLife::GameOfLife(int inpWidth, int inpHeight)
		:width(inpWidth), height(inpHeight)
	{
		if( width < 0 ){
			exit(0);
		}
		if (height < 0){
			exit(0);
		}
	}

	/**
	 *	set():	takes two integer, check validity of the input,
	 * 			and assign them to member variables width and heigth
	 *
	 *		inpWidth:	input for member variable width
	 *		inpHeight:	input for member variable height
	 *
	 *		width's value can't be lower than 0
	 *		height's value can't be lower than 0
	 *
	 */
	void GameOfLife::set(int inpWidth, int inpHeight ){
		if( inpWidth < 0 ){
			exit(0);
		}else {
			width = inpWidth;
		}
		if (inpHeight < 0){
			exit(0);
		}else {
			height = inpHeight;
		}
	}

	/**
	 *	setWidth():	takes an integer, check validity of the input,
	 * 				and assign it to the member variable width
	 *
	 *		inpWidth:	input for member variable width
	 *
	 *		width's value can't be lower than 0
	 *
	 */
	void GameOfLife::setWidth(int input){
		if (input < 0) {
			exit(0);
		}
		else{
			width = input;
		}
	}
	/**
	 *	setHeight():	takes an integer, check validity of the input,
	 * 					and assign it to member variable heigth
	 *
	 *		inpHeight:	input for member variable height
	 *
	 *		height's value can't be lower than 0
	 *
	 */
	void GameOfLife::setHeight(int input){
		if (input < 0) {
			exit(0);
		}
		else{
			height = input;
		}
	}

	/**
	 *	getHeight():	accessor of the member variable height
	 *
	 */
	int GameOfLife::getHeight()const{
		return height;
	}


	int GameOfLife::getSize()const{
		return size;
	}
		

	/**
	 *	getWidth():	accessor of the member variable width
	 *
	 */
	int GameOfLife::getWidth()const{
		return width;
	}
	/**
	 *	read():	takes a char* as argument. use this char* as an argument of open() function of fstream
	 *
	 *		fileName: char* keeps name of file which will be read
	 *
	 *		function reads the file and find living cells
	 *
	 *		file format:	file must include two int at the beginning of the file
	 *						first int will be value of the width member
	 *						second int will be value of the height member.
	 *						file will continue for height lines
	 *						and each line has width chars. chars must be only
	 *						' '(space) and 'x'
	 */
	void GameOfLife::read(char *fileName){
		ifstream inp;
		inp.open(fileName);

		inp >> width >> height;

		livingCell = new Cell[width*height];
		size = width*height;
		
		char* line = new char [width+1];
		inp.ignore(width,'\n');

		for(int i=0; i<height ; ++i ){
			inp.getline(line, width+1);//read line from file
			evaluate(line, i);
		}

		delete[]line;//to avoid memory leak
		inp.close();

	}

	/*
	 *
	 * operator over loads
	 *
	 */

	/**
	 *	operator[]:	takes an int named index. return a GameOfLife object which includes
	 *				livingCells only if they are at the same row which its number equal to index
	 *
	 *		doesn't works in const GameOfLife objects
	 */
	const GameOfLife GameOfLife::operator[](int index){
		GameOfLife row;

		for (int i = 0; i < size ; ++i) {
			if (livingCell[i].getY() == index) {
				row = row + livingCell[i] ;
			}
		}

		return row;
	}
	/**
	 *	operator[]:	takes an int named index. return a Cell
	 *				livingCells only if they are at the same row which its number equal to index
	 *
	 *		doesn't works in non-const GameOfLife objects
	 */

	Cell GameOfLife::operator[](int index)const{
		for (int i = 0; i < size; ++i) {
			if (livingCell[i].getX() == index) {
				return livingCell[i];
			}
		}

		return Cell(-1000, -1000);
	}


	/**
	 * operator():	takes two int, this argument represent a Cell objects coordinates
	 * 				in a GameOfLife object.
	 * 				if this Cell is alive returns same object
	 * 				else returns an object which its coordinates (-1000, -1000)
	 *
	 *
	 */
	Cell GameOfLife::operator()(int y, int x)const{
		if (isCellAlive(y, x)) {
			return Cell(x, y);
		}

		return Cell(-1000, -1000);
	}

	/**
	 *	operator+():	friend non member operator add. takes a GameOfLife and a
	 *					Cell reference returns a GameOfLife object which includes
	 *					whole elements of argument GameOflife object and also
	 *					argument  Cell object.
	 *
	 *		habitat:	argument typed GameOfLife reference.
	 *		individual:	argument typed Cell reference.
	 *
	 */
	const GameOfLife operator+(const GameOfLife& habitat, const Cell& individual){
		GameOfLife temp;
		temp = habitat;
		int i;
		
		if (!temp.isCellAlive(individual)) 
		{			
			temp.livingCell = new Cell[temp.size+1];
			
			for(i=0; i< temp.size ;++i)
				temp.livingCell[i] = habitat.livingCell[i];
			
			temp.livingCell[i] = individual;
			temp.size++;

			if (temp.getWidth() < individual.getX())
				temp.setWidth(individual.getX());

			if (temp.getHeight() < individual.getY())
					temp.setHeight(individual.getY());

		}

		return temp;
	}
	
	
	/**
	 *	operator-():	friend non member operator subtraction. takes a GameOfLife and a
	 *					Cell reference returns a GameOfLife object which includes
	 *					whole elements of argument GameOflife object except
	 *					argument  Cell object.
	 *
	 *		habitat:	argument typed GameOfLife reference.
	 *		individual:	argument typed Cell reference.
	 *
	 */
	const GameOfLife operator-(const GameOfLife& habitat, const Cell& individual){
		GameOfLife temp;
		temp = habitat;

		for (int i = 0; i < temp.size; ++i) {
			if (temp.livingCell[i] == individual) {			
				temp.erase(i);
			}
		}

		return temp;
	}

	/**
	 *	operator+():	friend non member operator add. takes two GameOfLife
	 *					references. returns a GameOfLife object which includes
	 *					whole elements of arguments first and second GameOflife objects.
	 *
	 *		first:	argument typed GameOfLife reference.
	 *		second:	argument typed GameOfLife reference.
	 *
	 */
	const GameOfLife operator+(const GameOfLife& first, const GameOfLife& second){
		GameOfLife temp;
		temp = first;
		for (int i = 0; i < second.getSize(); ++i) {
			if (!temp.isCellAlive(second.livingCell[i])) {
				temp.push_back(second.livingCell[i]);
			}
		}

		if(temp.getHeight() < second.getHeight() )
			temp.setHeight(second.getHeight());

		if (temp.getWidth() < second.getWidth())
			temp.setWidth(second.getWidth());

		return temp;
	}

	/**
	 *	operator-():	friend non member operator subtraction. takes two GameOfLife
	 *					references, returns a GameOfLife object which includes
	 *					whole elements of argument first GameOflife object except
	 *					argument second GameOfLife object.
	 *
	 *		first:	argument typed GameOfLife reference.
	 *		second:	argument typed GameOfLife reference.
	 *
	 */
	const GameOfLife operator-(const GameOfLife& first, const GameOfLife& second){
		GameOfLife temp;
		temp = first;
		for(int i = 0; i < temp.getSize() ; ++i) {
			for (int j = 0; j < second.getSize(); ++j) {
				if (temp.livingCell[i] == second.livingCell[j]) {
					
					temp.erase(i);
				}
			}
		}

		return temp;
	}
	/**
	 *	operator+()=:	member compound assignment operator. Takes a GameOfLife
	 *					object named arg. adds all elements of arg to original
	 *					GameOfLife object. returns a copy of the original
	 *					object
	 *
	 */
	const GameOfLife GameOfLife::operator+=(const GameOfLife& arg)
	{
		for (int i = 0; i < arg.getSize(); ++i) {
			if (!isCellAlive(arg.livingCell[i])) {
				this->push_back(arg.livingCell[i]);
			}
		}

		if(getHeight() < arg.getHeight() )
			setHeight(arg.getHeight());

		if (getWidth() < arg.getWidth())
			setWidth(arg.getWidth());

		return GameOfLife(*this);
	}

	/**
	 *	operator++():	prefix increment operator. creates next generation of the object
	 *					if a cell born out of borders of object extend the borders.
	 *					returns reference of the object.
	 *
	 */
	const GameOfLife& GameOfLife::operator++()
	{
		GameOfLife arg;

		history->push_game(GameOfLife(*this));
		
		extend();//extend habitat when a cell born out of borders
		
		arg = *this;
		
		this->size = width*height;
		this->livingCell = new Cell[size];
		
		int k=0;		
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				if (this->survive(i, j)) {
					arg.livingCell[k] = Cell(j,i);
				}
				k++;
			}
		}


		*this = arg;

		return *this;
	}
	/**
	 *	operator++():	postfix increment operator. creates next generation of the object
	 *					if a cell born out of borders of object extend the borders.
	 *					returns old generation of the object.
	 *
	 */
	GameOfLife GameOfLife::operator++(int)
	{
		GameOfLife arg;
		GameOfLife temp(*this);
		
		history->push_game(temp);
		extend();//extend habitat when a cell born out of borders
		
		arg = *this;

		int k=0;
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				if (survive(i, j)) {
					arg.livingCell[k] = Cell(j,i);
					}
				k++;
			}
		}
		
		*this = arg;

		return temp;
	}

	/**
	 *	operator<<():	stream insertion operator. write alive and dead cells of
	 *					the GameOfLife object to out stream. returns out stream
	 *
	 */
	ostream& operator<< (ostream& out, const GameOfLife& habitat){
		for (int i = 0; i < habitat.getHeight(); ++i) {
			out << "  ";
			for (int j = 0; j < habitat.getWidth(); ++j) {
				if(habitat.isCellAlive(i,j)){
					out << ALIVE <<" ";
				}
				else{
					out << DEAD << " ";
				}
			}
			out << endl;
		}

		return out;
	}

	/**
	 *	operator--():	prefix decrement operator. undo object one step
	 *					returns reference of the current version of the object.
	 *
	 *		if history of the object is empty terminates the program
	 */
	const GameOfLife& GameOfLife::operator--()
	{
		if ( aliveCell == 0) {
			cout << "\n\nThere is no older generation"<<endl;
			exit(0);
		}
		
		GameOfLife temp = *this;

		this->set( history[aliveCell-1].width, history[aliveCell-1].height);
		
		this->size = history[aliveCell-1].size;
		this->livingCell = new Cell[size];
		
		for( int i=0; i< size; ++i)
			this->livingCell[i] = temp.livingCell[i];
		
		history->pop_game();

	return *this;
	}



	/**
	 *	operator--():	postfix decrement operator. undo object one step
	 *					returns copy of the object before undo .
	 *		if history of the object is empty terminates the program
	 */
	GameOfLife GameOfLife::operator--(int i)
	{
		if ( aliveCell == 0 ) {
			cout << "\n\nThere is no older generation"<<endl;
			exit(0);
		}

		GameOfLife temp = *this;

		this->set( history[aliveCell-1].width, history[aliveCell-1].height);
		
		this->size = history[aliveCell-1].size;
		this->livingCell = new Cell[size];
		
		for( int i=0; i< size; ++i)
			this->livingCell[i] = temp.livingCell[i];
		
		history->pop_game();

		return temp;
	}



	/**
	 *
	 *
	 *
	 *	private member function implementations
	 *
	 *
	 *
	 *
	 */

	/**
	 *	evaluate():	takes a char* and an int and adds live members to
	 *				livingCells vector
	 *
	 *		line:	a line from the file which has just been read
	 *		height:	height of the living cells
	 */
	void GameOfLife::evaluate(char *line, int height)
	{
		for (int i = 0; i < getWidth() ; ++i) {
			if(line[i] == ALIVE){
				this->push_back(Cell(i, height));
			}
		}
	}

	/**
	 *	extend():	function which make width or height or both bigger
	 *				when a cell object born in out of the GameOfLife object borders
	 *
	 */
	void GameOfLife::extend()
	{
		extendToLeft();
		extendToRigth();
		extendToUp();		
		extendToDown();
	}

	/**
	 *	extendToLeft():	function which extends width of the GameOfLife object
	 *
	 *		adds 1 to all cells x and width
	 */
	void GameOfLife::extendToLeft(){
		bool temp = false;

		for (int i = 0; i < height; ++i) {
			if (survive(i, -1)) {
				temp = true;
				break;
			}
		}
		if (temp == true) {
			setWidth(width + 1);
			
			
			GameOfLife temp = *this;
			
			temp.width++;
			temp.size = width*height;
			temp.livingCell = new Cell[size];
			
			
			for(int i=0; i<size; ++i)
			{
				if(i%width != 0)
					temp.livingCell[i] = this->livingCell[i];
			}
			
			size = temp.size;
			this->livingCell = new Cell[size];
			
			
			for(int i=0; i<size; ++i)
				this->livingCell[i] = temp.livingCell[i];
			
		}
	}

	/**
	 *	extendToRight():	function which extend board to right
	 *
	 *		adds 1 to width
	 */
	void GameOfLife::extendToRigth(){
		bool temp = false;

		for (int i = 0; i < height; ++i) {
			if (survive(i, width + 1)) {
				temp = true;
				break;
			}
		}
		if (temp == true) {
			setWidth(width + 1);
			
			
			GameOfLife temp = *this;
			
			temp.width++;
			temp.size = width*height;
			temp.livingCell = new Cell[size];
			
			
			for(int i=0; i<size; ++i)
			{
				if(i%width != width-1)
					temp.livingCell[i] = this->livingCell[i];
			}
			
			size = temp.size;
			this->livingCell = new Cell[size];
			
			
			for(int i=0; i<size; ++i)
				this->livingCell[i] = temp.livingCell[i];		
		}
	}
	/**
	 *	extendToUp():	function which extend board to up
	 *
	 *		adds 1 to all cells y and height
	 */
	void GameOfLife::extendToUp(){
		bool flag = false;

		for (int i = 0; i < width; ++i) {
			if (survive(-1, i)) {
				flag = true;
				break;
			}
		}

		if (flag == true) {
			setHeight(getHeight() + 1);		
			GameOfLife temp = *this;
			
			temp.height++;
			temp.size = width*height;
			temp.livingCell = new Cell[size];
			
			
			this->livingCell = new Cell[size+width];
			size= temp.size;

			for(int i=width; i<size; ++i)
				this->livingCell[i] = temp.livingCell[i];
		
	
		}
	}
	/**
	 *	extendToDown():	function which extend board to down
	 *
	 *		adds 1 to height
	 */
	void GameOfLife::extendToDown(){
		bool flag = false;

		for (int i = 0; i < getWidth(); ++i) {
			if (survive(-1, i)) {
				flag = true;
				break;
			}
		}

		if (flag == true) {
			
			setHeight(getHeight() + 1);
			
			GameOfLife temp = *this;
			
			temp.height++;
			temp.size = width*height;
			temp.livingCell = new Cell[size];
			
			
			for(int i=0; i<size; ++i)
				temp.livingCell[i] = this->livingCell[i];
			
			
			this->livingCell = new Cell[size+width];
			
			for(int i=0; i<size; ++i)
				this->livingCell[i] = temp.livingCell[i];
			size= temp.size;
		}
	}

	/**
	 *	survive():	function which calculates if a cell will be alive
	 *				in the next generation,
	 *
	 *				if it will be alive returns true
	 *				else return false
	 *
	 *
	 */
	bool GameOfLife::survive(int y, int x){

		int neighbour ;

		neighbour = countNeighbours(y, x);

		/*	file: "formul_1_Gameoflife::survive.JPG"
		 *  explains condition of this is statement*/
		if(neighbour == 3 || (isCellAlive(y,x) && neighbour ==2)){
			return true;
		}


		return false;
	}
	/**
	 *	countNeighbours():	takes two int as coordinates of a Cell object.
	 *						counts alive neighbours of this object
	 *						and returns the count.
	 */
	int GameOfLife::countNeighbours(int y, int x)
	{

		int neighbour=0;

		for (int i = y-1; i <= y+1; ++i) {
			for (int j = x-1; j <= x+1; ++j) {
				if (isCellAlive(i,j) && !( y==i && x ==j)) {
					++neighbour;
				}
			}
		}

		return neighbour;
	}
	/**
	 *	isCellAlive():	takes two int and checks if the cell
	 *					in this coordinates if alive.
	 *
	 *					if the cell is alive returns true
	 *					or else returns false
	 *
	 */
	bool GameOfLife::isCellAlive(int y, int x)const{

		return isCellAlive(Cell(x, y));
	}


	/**
	 *	isCellAlive():	takes a Cell checks if the cell is alive.
	 *
	 *					if the cell is alive returns true
	 *					or else returns false
	 *
	 */
	bool GameOfLife::isCellAlive(Cell arg)const{
		for (int i = 0; i < (int) size ; ++i) {
			if(livingCell[i] == arg){
				return true;
			}
		}
		return false;
	}


}// namespace end
