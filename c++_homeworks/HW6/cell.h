/*
 * 
 * BIL241 HW06
 * 
 * FATMA CELIK
 *  101044038
 * 
 * 
 * */
#ifndef CELL_H
#define	CELL_H


#include <iostream>
#include <fstream>
#include <cstdlib>//exit()
#include <istream>


#define DEAD '.'
#define ALIVE 'x'

using namespace std;

namespace FatmaCelik
 {
	
	/*
	 *
	 * 	class Cell prototype
	 *
	 */
	class Cell {
		public:
			//constructors
			Cell();
			Cell(int);
			Cell(int, int);

			//mutators
			void set(int, int);
			void setX(int);
			void setY(int);

			//accessor
			int getX()const;
			int getY()const;

			/**
			 * operator overloads
			 *
			 */
			// compare operators
			bool operator<(const Cell&)const;
			bool operator>(const Cell&)const;
			bool operator<=(const Cell&)const;
			bool operator>=(const Cell&)const;
			bool operator!=(const Cell&)const;
			bool operator==(const Cell&)const;

			Cell& operator=(const Cell&);

			//pre pos decrement operators
			const Cell operator++();
			const Cell operator++(int);

			//pre pos increment operators
			const Cell operator--();
			const Cell operator--(int);

			// stream operators
			friend ostream& operator <<(ostream&, const Cell&);
			friend istream& operator >>(istream&, Cell& );


		private:
			int x;
			int y;
	};


}//namespace end
#endif
